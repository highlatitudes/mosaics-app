angular.module('mosaicsControllers').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/about.html',
    "<p>This is the about view.</p>\n"
  );


  $templateCache.put('views/admin/server.html',
    "<div>\n" +
    "    <input class=\"btn btn-danger\"\n" +
    "           type=\"button\"\n" +
    "           ng-confirm-click=\"Are you sure you want to clean DB?\"\n" +
    "           ng-click=\"clean()\"\n" +
    "           value=\"Clean DB\"/>\n" +
    "\n" +
    "    <input class=\"btn btn-primary\"\n" +
    "           type=\"button\"\n" +
    "           ng-click=\"initdata()\"\n" +
    "           value=\"Init DB with sample data\"/>\n" +
    "</div>"
  );


  $templateCache.put('views/admin/users.html',
    "<div>\n" +
    "    <div ng-repeat=\"user in users\">\n" +
    "        {{user.displayName}}\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('views/annotationActions.html',
    "<div class='actionBar' ng-show=\"hasPermission('write', annotation.data)\">\n" +
    "    <div>\n" +
    "        <span class='action glyphicon glyphicon-edit'\n" +
    "               ng-click='editAnnotationOnClick($event)'/></div>\n" +
    "    <div>\n" +
    "        <span class='action glyphicon glyphicon-remove'\n" +
    "               ng-click='deleteAnnotationOnClick($event)'\n" +
    "               ng-confirm-click=\"Are you sure you want to delete annotation?\"/></div>\n" +
    "</div>\n"
  );


  $templateCache.put('views/annotationDetails.html',
    "<div class='details'><b>{{annotation.name}}</b>\n" +
    "    <div ng-if=\"linkTypeName\">[{{linkTypeName}}]</div>\n" +
    "    <div ng-if=\"annotation.description\">{{annotation.description}}</div>\n" +
    "</div>\n"
  );


  $templateCache.put('views/annotationFilter.html',
    "<div>\n" +
    "    <div class=\"modal-header\">\n" +
    "        <h3 class=\"modal-title\">Filter Annotations</h3>\n" +
    "    </div>\n" +
    "    <div class=\"modal-body\">\n" +
    "        <div class=\"form-group row\" ng-show=\"currentUser\">\n" +
    "            <label class=\"col-sm-2 col-form-label\">Owner</label>\n" +
    "            <div class=\"col-sm-9\">\n" +
    "                <input type=\"radio\" ng-model=\"filter.owner\" value=\"me\">Mine\n" +
    "                <input type=\"radio\" ng-model=\"filter.owner\" value=\"\">All\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <hr>\n" +
    "\n" +
    "        <div class=\"form-group row\">\n" +
    "            <label class=\"col-sm-2 col-form-label\">Link type</label>\n" +
    "            <span id=\"linkTypeContainer\" style=\"position: relative\" class=\"col-sm-9\">\n" +
    "                <input class=\"form-control\" id=\"linkType\" ng-model=\"filter.body['moz:linkType']\" type=\"text\"/>\n" +
    "                <div class=\"spinner\"\n" +
    "                     style=\"position: absolute;right: 3em;top: 1.5em; display: none\">\n" +
    "                    <div class=\"rect1\"></div>\n" +
    "                    <div class=\"rect2\"></div>\n" +
    "                    <div class=\"rect3\"></div>\n" +
    "                </div>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "\n" +
    "        <hr>\n" +
    "        <div class=\"form-group row\">\n" +
    "            <label class=\"col-sm-2 col-form-label\">From</label>\n" +
    "            <span id=\"annSourceContainer\" style=\"position: relative\" class=\"col-sm-9\">\n" +
    "                <search-dropdown sources=\"autocompleteSources\" ng-model=\"filter.target\" display-field=\"source\"/>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "\n" +
    "        <hr>\n" +
    "        <div class=\"form-group row\">\n" +
    "            <label class=\"col-sm-2 col-form-label\">To</label>\n" +
    "            <span id=\"annTargetContainer\" style=\"position: relative\" class=\"col-sm-9\">\n" +
    "                <search-dropdown sources=\"autocompleteSources\" ng-model=\"filter.body\" display-field=\"source\"/>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"modal-footer\">\n" +
    "        <input class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\" value=\"Filter\"/>\n" +
    "        <input class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\" value=\"Cancel\"/>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('views/annotationOverlay.html',
    "<div>\n" +
    "    <div class='details'><b>{{annotation.name}}</b>\n" +
    "        <div ng-if=\"linkTypeName\">[{{linkTypeName}}]</div>\n" +
    "        <div ng-if=\"annotation.description\">{{annotation.description}}</div>\n" +
    "    </div>\n" +
    "    <div class='actionBar'>\n" +
    "        <div><span class='action glyphicon glyphicon-edit'\n" +
    "                   ng-click='editAnnotationOnClick($event)'/></div>\n" +
    "        <div><span class='action glyphicon glyphicon-remove'\n" +
    "                   ng-click='deleteAnnotationOnClick($event)'\n" +
    "                   ng-confirm-click=\"Are you sure you want to delete annotation?\"/></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('views/annotationSearchResult.html',
    "\n" +
    "<div ng-init=\"mozTitle = ann['moz:inMosaic']['dc:title']; linkType = ann.body['moz:linkType']; res = ann['dct:references']\"\n" +
    "     style=\"display: table-row\">\n" +
    "    <div style=\"display:table-cell; text-align: right; white-space: nowrap\">\n" +
    "        <a ng-if=\"mozTitle\" ui-sref=\"mosaics.id({id:(ann['moz:inMosaic'].id || ann['moz:inMosaic'])})\">\n" +
    "            {{mozTitle}}\n" +
    "        </a>\n" +
    "        <a ng-if=\"res['dc:title']\" ui-sref=\"mosaics.id.resource({id:(ann['moz:inMosaic'].id || ann['moz:inMosaic']),res:res.id})\"> / {{res['dc:title']}}</a>\n" +
    "        <span ng-if=\"!(mozTitle || res['dc:title'])\"\n" +
    "           title=\"{{ann.target.source}}\"\n" +
    "           style=\"font-family: monospace;font-size: 90%;\">\n" +
    "            &lt;<a ui-sref=\"preview({url:ann.target.source})\">{{truncateUri(ann.target.source, 50)}}</a>&gt;\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div style=\"display:table-cell; padding: 0px 5px; white-space: nowrap\">\n" +
    "        <i ng-class=\"getDimensionClass(targetDimension)\" title=\"{{targetMimeType}}\"></i>\n" +
    "        <i ng-class=\"getDimensionClass(bodyDimension)\" title=\"{{bodyMimeType}}\"></i>\n" +
    "    </div>\n" +
    "    <div style=\"display:table-cell; text-align: left; white-space: nowrap\">\n" +
    "        <span ng-if=\"linkType\"\n" +
    "              style=\"font-family: monospace;font-size: 90%;\"\n" +
    "              title=\"{{linkType}}\">\n" +
    "            &lt;{{getLinkName(linkType)}}&gt;\n" +
    "        </span>\n" +
    "        <a ng-if=\"ann['moz:inMosaic']\" ui-sref=\"mosaics.id.annotation({id:(ann['moz:inMosaic'].id || ann['moz:inMosaic']), ann:ann.id, open:true})\">{{ann['dc:title']}}</a>\n" +
    "        <a ng-if=\"!ann['moz:inMosaic']\" ui-sref=\"preview({url:ann.target.source, ann:ann.id, open:true})\">{{ann['dc:title']}}</a>\n" +
    "\n" +
    "    </div>\n" +
    "    <div style=\"display:table-cell; text-align: left; padding-left: 10px; white-space: nowrap\">\n" +
    "        {{ (ann['modified'] || ann['moz:inMosaic']['modified'])| date:'short'}}\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('views/appErrors.html',
    "<div class=\"appErrorList alert alert-warning\" ng-show=\"appErrors.new_errors.length && showErrors\">\n" +
    "    <span style=\"float: right\" class=\"glyphicon glyphicon-remove\" ng-click=\"toggleDisplay(false)\"></span>\n" +
    "    <div class=\"content\">\n" +
    "        <div ng-repeat=\"error in appErrors.new_errors\" class=\"error\">\n" +
    "            <span class=\"glyphicon glyphicon-exclamation-sign\"></span>\n" +
    "            {{error.status}} {{error.statusText}} : {{error.data.message}}\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('views/captureActionBar.html',
    "<div class=\"actionBar shrinkable\">\n" +
    "\n" +
    "    <div>\n" +
    "        <span class=\"action-label\">Mosaic</span>\n" +
    "        <span class=\"action action-add-mosaic glyphicon glyphicon-comment\" ng-click=\"capture(fragment)\"/>\n" +
    "    </div>\n" +
    "    <!-- TODO implement clipboard action -->\n" +
    "\n" +
    "    <!--\n" +
    "        <div>\n" +
    "            <span class=\"action-label\">Clipboard</span>\n" +
    "\n" +
    "            <span class=\"action action-add-clipboard glyphicon glyphicon-plus\" ng-click=\"capture(fragment)\"/>\n" +
    "        </div>\n" +
    "        -->\n" +
    "\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('views/disclaimer.html',
    "<div ng-show=\"currentUser && !getCookie('disclaimerRead')\" class=\"disclaimer alert alert-info\">\n" +
    "    <div class=\"close_button glyphicon glyphicon-remove\" ng-click=\"setCookie('disclaimerRead', true)\"></div>\n" +
    "    <b style=\"margin-right: 10px\">Disclaimer</b> This application is for demo purposes. All user-added data is public, and its content may be\n" +
    "    discarded at any time.\n" +
    "</div>"
  );


  $templateCache.put('views/displayActionBar.html',
    "<i ng-if=\"allowGraphDisplay\" class=\"icon-graph\" ng-click=\"toggleGraphDisplay(true)\"></i>\n" +
    "<div class=\"btn-group dropdown-hover\" style=\"vertical-align: text-top\" >\n" +
    "    <span class='glyphicon-comment glyphicon' ng-click=\"toggleAnnotations()\" ng-style=\"{color: annotationsDisplayEnabled?'#83e683':undefined}\"></span>\n" +
    "    <div ng-if=\"filterAnnotationsDialog\" class=\"dropdown-menu\" style=\"font-size: inherit; min-width: 0; position: absolute; z-index: 1000; border: none; padding: 3px; left: -3px\">\n" +
    "        <div class=\"action dropdown-item glyphicon-filter glyphicon\" ng-click=\"filterAnnotationsDialog(currentResource && currentResource.source)\"></div>\n" +
    "    </div>\n" +
    "</div>\n"
  );


  $templateCache.put('views/doc.html',
    "<cell class=\"col-md-2\"/>\n" +
    "<cell class=\"col-md-8 app-doc\">\n" +
    "    <!--\n" +
    "    <div data-spy=\"affix\" class=\"left-menu\">\n" +
    "        <img src=\"images/logo-big.png\" >\n" +
    "    </div>\n" +
    "    -->\n" +
    "    <div class=\"left-menu affix\">\n" +
    "        <ul class=\"nav nav-pills nav-stacked\">\n" +
    "            <li role=\"presentation\"><a href=\"/#doc#overview\">Overview</a></li>\n" +
    "            <li role=\"presentation\"><a href=\"/#doc#features\">Features</a></li>\n" +
    "            <li role=\"presentation\"><a href=\"/#doc#usecases\">Use Cases</a></li>\n" +
    "            <li role=\"presentation\"><a href=\"/#doc#tech\">Technical</a></li>\n" +
    "            <li role=\"presentation\"><a href=\"/#doc#related\">Related Material</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "    <div id=\"overview\">\n" +
    "        <img src=\"images/logo-big.png\" width=\"150px\"/>\n" +
    "        <div style=\"display: inline-block; vertical-align: top;\">\n" +
    "            <h2>Mosaics</h2>\n" +
    "            An online platform to visualize, annotate and mash up fragments of online media and data\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <p id=\"features\">\n" +
    "        <h4>Features</h4>\n" +
    "        <p>\n" +
    "            <h5>Online collaborative platform to search, author and visualize mosaics</h5>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <h5>Out of the box support for many media types</h5>\n" +
    "            Text, HTML, PDF, Video formats and streaming platforms, tabular data (CSV), geospatial formats (OGC family, AcrGIS)\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <h5>Extensible viewer framework</h5>\n" +
    "            Custom viewer/editor plugins can be added for any media type.\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <h5>W3C's Web Annotation support</h5>\n" +
    "            Mosaics model is based on W3C's Web Annotation model\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <h5>REST API</h5>\n" +
    "            Mosaics and annotations are exposed through a REST API\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <h5>Sparql endpoint</h5>\n" +
    "            As the internal storage is based on a triple store, content can be searched using sparql expressions,\n" +
    "            including geosparql support\n" +
    "        </p>\n" +
    "    </p>\n" +
    "\n" +
    "    <p id=\"usecases\">\n" +
    "        <h4>Use Cases</h4>\n" +
    "\n" +
    "    </p>\n" +
    "\n" +
    "    <p id=\"tech\">\n" +
    "        <h4>Technical</h4>\n" +
    "\n" +
    "    </p>\n" +
    "\n" +
    "    <p id=\"related\">\n" +
    "        <h4>Related Material</h4>\n" +
    "        <ul>\n" +
    "            <li>Data Mosaics Project deliverables, JRC Citizen Science Platform, March 2017\n" +
    "                <ul>\n" +
    "                   <li><a href=\"http://blog.highlatitud.es/docs/CCR.IES.C392667.X0-DataMosaics-D1.pdf\">D1: Technical background, usage scenarios and storyline of a demo</a></li>\n" +
    "                   <li><a href=\"http://blog.highlatitud.es/docs/CCR.IES.C392667.X0-DataMosaics-D2.pdf\">D2: Demonstrator Documentation</a></li>\n" +
    "                   <li><a href=\"http://blog.highlatitud.es/docs/CCR.IES.C392667.X0-DataMosaics-FR.pdf\">Final Report</a></li>\n" +
    "                </ul>\n" +
    "            </li>\n" +
    "            <li><a href=\"http://www.w3.org/2014/03/lgd/papers/lgd14_submission_56\">Stitching data mashups from geodata fragments</a>, LinkedGeoData Workshop - W3C, March 2014</li>\n" +
    "            <li><a href=\"http://www.w3.org/2013/04/odw/odw13_submission_28.pdf\">Aggregating media fragments into collaborative mashups: standards and a prototype</a>, Open Data on the Web Workshop - W3C, April 2013</li>\n" +
    "        </ul>\n" +
    "    </p>\n" +
    "\n" +
    "</cell>\n" +
    "<cell class=\"col-md-2\"/>\n" +
    "<script>\n" +
    "    $('[data-spy=\"affix\"]').affix({\n" +
    "        offset: {\n" +
    "            top: 200\n" +
    "        }\n" +
    "    })\n" +
    "\n" +
    "    $('.app-container').scrollspy({ target: '.app-doc .left-menu' })\n" +
    "</script>\n"
  );


  $templateCache.put('views/editAnnotation.html',
    "<div>\n" +
    "    <div class=\"modal-header\">\n" +
    "        <span class=\"btn btn-primary btn-sm\" style=\"float:right\" ng-click=\"editMode = !editMode\">JSON</span>\n" +
    "        <h3 class=\"modal-title\">Edit Annotation</h3>\n" +
    "    </div>\n" +
    "    <div class=\"modal-body\">\n" +
    "    <div class=\"flipContainer\" ng-class=\"{showBack: editMode}\">\n" +
    "        <div class=\"flipper\">\n" +
    "            <div class=\"mosaicEditor back\">\n" +
    "                <ui-codemirror ui-codemirror-opts=\"editorOptions\" ng-model=\"jsonEditedAnn\" ui-refresh=\"editMode\"></ui-codemirror>\n" +
    "            </div>\n" +
    "            <div class=\"front\">\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Title</label>\n" +
    "                        <div class=\"col-sm-10\"><input ng-model=\"editedAnn['dc:title']\" class=\"form-control\"/></div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Link type</label>\n" +
    "            <span id=\"linkTypeContainer\" style=\"position: relative\" class=\"col-sm-9\">\n" +
    "                <input class=\"form-control\" id=\"linkType\" ng-model=\"editedAnn.body['moz:linkType']\" type=\"text\"/>\n" +
    "                <div class=\"spinner\"\n" +
    "                     style=\"position: absolute;right: 3em;top: 1.5em; display: none\">\n" +
    "                    <div class=\"rect1\"></div>\n" +
    "                    <div class=\"rect2\"></div>\n" +
    "                    <div class=\"rect3\"></div>\n" +
    "                </div>\n" +
    "            </span>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <hr>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">From</label>\n" +
    "            <span class=\"col-sm-9\">\n" +
    "                <search-dropdown sources=\"autocompleteSources\" ng-model=\"editedAnn.target\" display-field=\"source\"/>\n" +
    "            </span>\n" +
    "                        <span class=\"col-sm-1 glyphicon glyphicon-eye-open\" alt=\"Preview\" ng-click=\"preview(editedAnn.target)\" style=\"font-size: 120%; vertical-align: bottom;\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Fragment</label>\n" +
    "                        <div class=\"col-sm-10\"><input ng-model=\"editedAnn.target.selector.value\" class=\"form-control\"/></div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Format</label>\n" +
    "                        <div class=\"col-sm-10\"><input ng-model=\"editedAnn.target.format\" class=\"form-control\"/></div>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <hr>\n" +
    "                    <span class=\"glyphicon glyphicon-sort\" ng-click=\"switch()\"></span>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">To</label>\n" +
    "            <span class=\"col-sm-9\">\n" +
    "                <search-dropdown sources=\"autocompleteSources\" ng-model=\"editedAnn.body\" display-field=\"source\"/>\n" +
    "            </span>\n" +
    "                        <span class=\"col-sm-1 glyphicon glyphicon-eye-open\" alt=\"Preview\" ng-click=\"preview(editedAnn.body)\" style=\"font-size: 120%; vertical-align: bottom;\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Fragment</label>\n" +
    "                        <div class=\"col-sm-10\"><input ng-model=\"editedAnn.body.selector.value\" class=\"form-control\"/></div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Format</label>\n" +
    "                        <div class=\"col-sm-10\"><input ng-model=\"editedAnn.body.format\" class=\"form-control\"/></div>\n" +
    "                    </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    </div>\n" +
    "    <div class=\"modal-footer preview\" ng-if=\"previewedRes && previewedRes.source\">\n" +
    "        <div resource-display ng-res=\"previewedRes\" capture-fragment-fn=\"captureFragment\" style=\"height: 200px; position:relative\"/>\n" +
    "    </div>\n" +
    "    <div class=\"modal-footer\">\n" +
    "        <input class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\" value=\"Save\"/>\n" +
    "        <input class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\" value=\"Cancel\"/>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('views/editMosaic.html',
    "<div>\n" +
    "    <div class=\"modal-header\">\n" +
    "        <h3 class=\"modal-title\">Edit Mosaic</h3>\n" +
    "    </div>\n" +
    "    <div class=\"modal-body\">\n" +
    "                <div class=\"form-group row\">\n" +
    "                    <label class=\"col-sm-2 col-form-label\">Title</label>\n" +
    "                    <div class=\"col-sm-10\"><input ng-model=\"editedMoz['dc:title']\" class=\"form-control\"/></div>\n" +
    "                </div>\n" +
    "\n" +
    "                <hr>\n" +
    "    </div>\n" +
    "    <div class=\"modal-footer\">\n" +
    "        <input class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\" value=\"Save\"/>\n" +
    "        <input class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\" value=\"Cancel\"/>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('views/editResource.html',
    "<div>\n" +
    "    <div class=\"modal-header\">\n" +
    "        <span class=\"btn btn-primary btn-sm\" style=\"float:right\" ng-click=\"editMode = !editMode\">JSON</span>\n" +
    "        <h3 class=\"modal-title\">Edit Resource</h3>\n" +
    "    </div>\n" +
    "    <div class=\"modal-body\">\n" +
    "    <div class=\"flipContainer\" ng-class=\"{showBack: editMode}\">\n" +
    "        <div class=\"flipper\">\n" +
    "            <div class=\"mosaicEditor back\">\n" +
    "                <ui-codemirror ui-codemirror-opts=\"editorOptions\" ng-model=\"jsonEditedRes\" ui-refresh=\"editMode\"></ui-codemirror>\n" +
    "            </div>\n" +
    "            <div class=\"front\">\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Title</label>\n" +
    "                        <div class=\"col-sm-10\"><input ng-model=\"editedRes['dc:title']\" class=\"form-control\"/></div>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <hr>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">URL</label>\n" +
    "            <span class=\"col-sm-9\">\n" +
    "                <search-dropdown sources=\"autocompleteSources\" ng-model=\"editedRes\" display-field=\"source\"/>\n" +
    "            </span>\n" +
    "                        <span class=\"col-sm-1 glyphicon glyphicon-eye-open\" alt=\"Preview\" ng-click=\"preview(editedRes)\" style=\"font-size: 120%; vertical-align: bottom;\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Fragment</label>\n" +
    "                        <div class=\"col-sm-10\"><input ng-model=\"editedRes.selector.value\" class=\"form-control\"/></div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group row\">\n" +
    "                        <label class=\"col-sm-2 col-form-label\">Format</label>\n" +
    "                        <div class=\"col-sm-10\"><input ng-model=\"editedRes.format\" class=\"form-control\"/></div>\n" +
    "                    </div>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    </div>\n" +
    "    <div class=\"modal-footer preview\" ng-if=\"previewedRes && previewedRes.source\">\n" +
    "        <div resource-display ng-res=\"previewedRes\" capture-fragment-fn=\"captureFragment\" style=\"height: 200px; position:relative\"/>\n" +
    "    </div>\n" +
    "    <div class=\"modal-footer\">\n" +
    "        <input class=\"btn btn-primary\" type=\"button\" ng-click=\"ok()\" value=\"Save\"/>\n" +
    "        <input class=\"btn btn-warning\" type=\"button\" ng-click=\"cancel()\" value=\"Cancel\"/>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('views/main.html',
    "<div >\n" +
    "\n" +
    "</div>\n"
  );


  $templateCache.put('views/preview.html',
    "<cell overlaid-annotations=\"annotationFilter\" resource-context mosaic mosaic-var=\"tempMosaic\">\n" +
    "    <div class=\"form-group row\">\n" +
    "        <div class=\"col-sm-10\">\n" +
    "            <search-dropdown sources=\"autocompleteSources\"\n" +
    "                             ng-model=\"formResource\"\n" +
    "                             display-field=\"source\"\n" +
    "                             ng-enter=\"preview()\"/>\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-2\" >\n" +
    "        <select ng-model=\"requiredMimeType\"\n" +
    "                ng-options=\"mt as ext for (mt, ext) in typeMap\"\n" +
    "                ng-change=\"preview(requiredMimeType)\"\n" +
    "                class=\"form-control\"></select>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div style=\"display: table; height: 100%\"><row>\n" +
    "        <div class=\"slidein panel left\">\n" +
    "            <div style=\"position: absolute\">\n" +
    "                <div id=\"annotationsList\" ng-if=\"selectedRes\" class=\"subpanel\" ng-init=\"displayFrom = true\">\n" +
    "                    <div class=\"panel_header\">\n" +
    "                        <h5>Annotations\n" +
    "                            <span style=\"float:right\" class=\"action glyphicon glyphicon-cog\" ng-click=\"editAnnotationFilterModal()\"></span>\n" +
    "                        </h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel_content\" >\n" +
    "                        <div ng-if=\"filteredAnnotations.length>0\" class=\"annotList\" ng-repeat=\"annot in filteredAnnotations\">\n" +
    "                            <span class=\"glyphicon\" ng-class=\"annot.body && annot.body.source ? 'glyphicon-log-out' : 'glyphicon-comment' \"/>\n" +
    "                            <span ng-click=\"gotoAnnotation(annot)\" ng-mouseover=\"highlightAnnotation(annot, true)\" ng-mouseleave=\"highlightAnnotation(annot, false)\">{{annot['dc:title']}}</span>\n" +
    "                            <span class=\"action glyphicon glyphicon-trash\" ng-click=\"deleteAnnotationFromMosaic(selected, annot)\" ng-confirm-click=\"Are you sure you want to delete annotation?\"></span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"panel\">\n" +
    "            <div style=\"padding: 0px 10px\" class=\"layout-right\">\n" +
    "            <div class=\"mosaicDisplay\" style=\"height: calc(100% - 34px - 15px);\"  mosaic-display\n" +
    "                 ng-res=\"selectedRes\"\n" +
    "                 annotations-from-map=\"annotationsFromMap\"\n" +
    "                 annotations-to-map=\"annotationsToMap\"\n" +
    "                 filtered-annotations=\"filteredAnnotations\"\n" +
    "                 filter-annotations-dialog=\"editAnnotationFilterModal\">\n" +
    "                <div ng-show=\"displayGraph\" class=\"mosaicGraph\" style=\"height: 100%\">\n" +
    "                    <div class=\"hoverMenu widget\">\n" +
    "                        <i class=\"glyphicon-th-large glyphicon\" ng-click=\"toggleGraphDisplay(false)\"></i>\n" +
    "                    </div>\n" +
    "                    <div class=\"graphContainer\" style=\"height: 100%;\"></div>\n" +
    "                </div>\n" +
    "                <div ng-show=\"!displayGraph\" class=\"mainResource\" style=\"height: 100%\">\n" +
    "                    <div resource-display class=\"mainResViewer\" ng-res=\"displayedResource\"\n" +
    "                         annotations-from-map=\"annotationsFromMap\"\n" +
    "                         annotations-to-map=\"annotationsToMap\"\n" +
    "                         annotation-renderer=\"annotationRenderer\"\n" +
    "                         capture-fragment-fn=\"captureFragmentFn\"\n" +
    "                         filtered-annotations=\"filteredAnnotations\"\n" +
    "                         filter-annotations-dialog=\"filterAnnotationsDialog\"\n" +
    "                         allow-graph-display=\"true\">\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"linkedResources\" ng-show=\"$root.keys(displayedAnnotationResources).length > 0\">\n" +
    "                    <div class=\"annotResContainer\" ng-repeat=\"(hash, ann) in displayedAnnotationResources\">\n" +
    "                        <div resource-display class=\"annotResViewer\" ng-res=\"ann\" close-viewer-fn=\"closeAnnotationResource\">\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        </div>\n" +
    "    </row></div>\n" +
    "</cell>"
  );


  $templateCache.put('views/profile.html',
    "<cell>\n" +
    "    <div class=\"form-group row\">\n" +
    "        <label class=\"col-sm-2 col-form-label\">Username</label>\n" +
    "        <div class=\"col-sm-10\"><input ng-model=\"currentUser.principal.user.username\" class=\"form-control\"/></div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group row\">\n" +
    "        <label class=\"col-sm-2 col-form-label\">Profiles</label>\n" +
    "        <div class=\"col-sm-10\">\n" +
    "            <div class=\"form-group row\" ng-repeat=\"provider in currentUser.principal.user.providerProfiles\">\n" +
    "                <label class=\"col-sm-2 col-form-label\">{{provider.provider}}</label>\n" +
    "                <div class=\"col-sm-10\"><input ng-model=\"provider.id\" class=\"form-control\"/></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</cell>"
  );


  $templateCache.put('views/query.html',
    "<cell mosaics mosaic mosaic-var=\"selected\">\n" +
    "    <div style=\"display: table; height: 100%\"><row>\n" +
    "    <div class=\"slidein panel left\">\n" +
    "        <h3 class=\"header_padder\"><!-- vertical padding--></h3>\n" +
    "        <div style=\"position: absolute\">\n" +
    "            <div id=\"mosaicList\" class=\"subpanel\">\n" +
    "                <ul class=\"nav nav-tabs panel_header\">\n" +
    "                    <li><h5 style=\"margin-top: 9px;margin-bottom: 0px;margin-right: 3px\">Mosaics</h5></li>\n" +
    "                    <li role=\"presentation\" ui-sref-active=\"active\" ui-sref=\"mosaics.query({query: 'mine'})\"><a >Mine</a></li>\n" +
    "                    <li role=\"presentation\" ui-sref-active=\"active\" ui-sref=\"mosaics.all\"><a >All</a></li>\n" +
    "                    <li style=\"float: right\"><span title=\"Create a Mosaic\" class=\"action btn glyphicon glyphicon-plus\" style=\"float: right\" ng-click=\"createMosaic()\"></span></li>\n" +
    "                </ul>\n" +
    "                <div class=\"panel_content\" >\n" +
    "                    <div ng-repeat=\"mosaic in mosaics\">\n" +
    "                        <!-- <span  ng-click=\"select(mosaic)\" ng-style=\"{ 'font-weight': isSelected(mosaic) ? 'bold' : '' }\">{{mosaic[\"dc:title\"] || mosaic[\"id\"]}}</span> -->\n" +
    "                        <span class=\"link\" ui-sref=\"mosaics.id(mosaic)\" ui-sref-active=\"active\">{{mosaic[\"dc:title\"] || mosaic[\"id\"]}}</span>\n" +
    "                        <span class=\"action glyphicon glyphicon-trash\" ng-show=\"hasPermission('delete', mosaic)\" ng-click=\"deleteMosaic(mosaic)\" ng-confirm-click=\"Are you sure you want to delete mosaic?\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div id=\"resourceList\" ng-if=\"selected\" class=\"subpanel\">\n" +
    "                <div class=\"panel_header\">\n" +
    "                    <div title=\"Add a ressource\" style=\"float: right\">\n" +
    "                        <span class=\"action glyphicon glyphicon-plus\" ng-click=\"createResource(selected)\"></span>\n" +
    "                    </div>\n" +
    "                    <h5>Resources</h5>\n" +
    "                </div>\n" +
    "                <div class=\"panel_content\" >\n" +
    "                    <div ng-repeat=\"res in selected.resources\">\n" +
    "                        <div>\n" +
    "                            <span class=\"action glyphicon glyphicon-edit\" ng-show=\"hasPermission('write', selected)\" ng-click=\"editResourceModal(res)\"></span>\n" +
    "                            <span class=\"action glyphicon glyphicon-trash\" ng-show=\"hasPermission('write', selected)\" ng-click=\"deleteResource(selected, res)\" ng-confirm-click=\"Are you sure you want to delete resource?\"></span>\n" +
    "                            <span class=\"link\" ui-sref=\"mosaics.id.resource({id:selected.id, res:res.id})\" ui-sref-active=\"active\">{{res['dc:title']}}</span>\n" +
    "                        </div>\n" +
    "                        <div ng-if=\"selectedRes == res\" class=\"annotList\" ng-repeat=\"annot in annotationsFromMap[res['source']]\">\n" +
    "                            <span class=\"glyphicon\" ng-class=\"annot.body && annot.body.source ? 'glyphicon-log-out' : 'glyphicon-comment' \"/>\n" +
    "                            <span ng-click=\"gotoAnnotation(annot)\" ng-mouseover=\"highlightAnnotation(annot, true)\" ng-mouseleave=\"highlightAnnotation(annot, false)\">{{annot['dc:title']}}</span>\n" +
    "                            <span class=\"action glyphicon glyphicon-trash\" ng-show=\"hasPermission('write', selected)\" ng-click=\"deleteAnnotationFromMosaic(selected, annot)\" ng-confirm-click=\"Are you sure you want to delete annotation?\"></span>\n" +
    "                        </div>\n" +
    "                        <div ng-if=\"selectedRes == res\" class=\"annotList\" ng-repeat=\"annot in annotationsToMap[res['source']]\">\n" +
    "                            <span class=\"glyphicon glyphicon-log-in\"/>\n" +
    "                            <span ng-click=\"gotoAnnotation(annot)\" ng-mouseover=\"highlightAnnotation(annot, true)\" ng-mouseleave=\"highlightAnnotation(annot, false)\">{{annot['dc:title']}}</span>\n" +
    "                            <span class=\"action glyphicon glyphicon-trash\" ng-show=\"hasPermission('write', selected)\" ng-click=\"deleteAnnotationFromMosaic(selected, annot)\" ng-confirm-click=\"Are you sure you want to delete annotation?\"></span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <!--\n" +
    "            <div id=\"annotationsList\" ng-if=\"selectedRes\" class=\"subpanel\" ng-init=\"displayFrom = true\">\n" +
    "                <div class=\"panel_header\">\n" +
    "                    <h5>Annotations\n" +
    "                        <span style=\"float:right\" class=\"action glyphicon glyphicon-cog\" ng-click=\"editAnnotationFilterModal()\"></span>\n" +
    "                    </h5>\n" +
    "                </div>\n" +
    "                <div class=\"panel_content\" >\n" +
    "                    <div ng-repeat=\"annot in (displayFrom?annotationsFromMap:annotationsToMap)[selectedRes['source']]\">\n" +
    "                        <span ng-click=\"gotoAnnotation(annot)\">{{annot['dc:title']}}</span>\n" +
    "                        <span class=\"action glyphicon glyphicon-trash\" ng-click=\"deleteResource(res)\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            -->\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel\">\n" +
    "    <div ng-show=\"selected\" style=\"padding: 0px 10px\" ng-init=\"layout = 'right'\" ng-class=\"'layout-'+layout\">\n" +
    "        <h3>\n" +
    "            <span editable ng-model=\"selected['dc:title']\"></span>\n" +
    "            <span ng-if=\"selectedRes\">\n" +
    "                / <span editable ng-model=\"selectedRes['dc:title']\"></span>\n" +
    "            </span>\n" +
    "            <div class=\"mosaic-action-bar\">\n" +
    "                <div class=\"btn-group\">\n" +
    "                    <span class=\"action dropdown-toggle\" ng-class=\"{i_right_panel: layout == 'right', i_layout_design: layout == 'bottom', i_four_grid: layout == 'grid'}\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
    "                    </span>\n" +
    "                    <div class=\"dropdown-menu\" style=\"font-size: inherit; min-width: 0; position: absolute; z-index: 1000; border: none; padding: 2px; left: -2px\">\n" +
    "                        <div class=\"action i_right_panel dropdown-item\" ng-click=\"layout = 'right'\"></div>\n" +
    "                        <div class=\"action i_layout_design dropdown-item\" ng-click=\"layout = 'bottom'\"></div>\n" +
    "                        <div class=\"action i_four_grid dropdown-item\" ng-click=\"layout = 'grid'\"></div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <span ng-if=\"selectedRes\">\n" +
    "                    <a title=\"Open original resource\" class=\"action glyphicon glyphicon-share\" ng-href=\"selectedRes['source']\"></a> |\n" +
    "                </span>\n" +
    "                <span title=\"Edit mosaic\" class=\"action glyphicon glyphicon-edit\" ng-show=\"hasPermission('write', getMosaic())\" ng-click=\"editMosaicModal(getMosaic())\"></span>\n" +
    "                <span title=\"Edit JSON\" class=\"action glyphicon glyphicon-json\" ng-click=\"toggleEdit()\"></span>\n" +
    "                <span ng-show=\"hasPermission('write', selected) && selected.$dirty\" class=\"action glyphicon glyphicon-floppy-save\" ng-click=\"saveMosaic(selected)\"></span>\n" +
    "            </div>\n" +
    "        </h3>\n" +
    "        <div class=\"flipContainer\" ng-class=\"{showBack: editMode}\">\n" +
    "            <div class=\"flipper\">\n" +
    "                <div class=\"mosaicEditor back\">\n" +
    "                    <div class=\"alert alert-warning\" style=\"margin-bottom: 1px;\" ng-show=\"!hasPermission('write', getMosaic())\">You do not have write permissions on this document. You can edit its JSON for test purposes, but won't be be able to save changes.</div>\n" +
    "                    <ui-codemirror id=\"mosaicEditor\" ui-codemirror-opts=\"editorOptions\" ng-model=\"jsonMosaic\"></ui-codemirror>\n" +
    "                </div>\n" +
    "                <div class=\"mosaicDisplay front\" mosaic-display ng-res=\"selectedRes\" annotations-from-map=\"annotationsFromMap\" annotations-to-map=\"annotationsToMap\" filtered-annotations=\"filteredAnnotations\" filter-annotations-dialog=\"editAnnotationFilterModal\">\n" +
    "                    <div class=\"mainResource\">\n" +
    "                        <div resource-display class=\"mainResViewer\"\n" +
    "                             ng-res=\"displayedResource\"\n" +
    "                             mosaic-display-ctrl=\"mosaicDisplayCtrl\"\n" +
    "                             annotations-from-map=\"annotationsFromMap\"\n" +
    "                             annotations-to-map=\"annotationsToMap\"\n" +
    "                             annotation-renderer=\"annotationRenderer\"\n" +
    "                             capture-fragment-fn=\"captureFragmentFn\"\n" +
    "                             allow-graph-display=\"true\">\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"linkedResources\" ng-show=\"$root.keys(displayedAnnotationResources).length > 0\">\n" +
    "                        <div class=\"annotResContainer\" ng-repeat=\"(hash, ann) in displayedAnnotationResources\">\n" +
    "                            <div resource-display class=\"annotResViewer\"\n" +
    "                                 ng-res=\"ann\"\n" +
    "                                 annotations-from-map=\"annotationsFromMap\"\n" +
    "                                 annotations-to-map=\"annotationsToMap\"\n" +
    "                                 close-viewer-fn=\"closeAnnotationResource\"\n" +
    "                                 allow-graph-display=\"false\">\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "    </row></div>\n" +
    "</cell>"
  );


  $templateCache.put('views/samples.html',
    "<div >\n" +
    "      <div>\n" +
    "\n" +
    "          <div ng-repeat=\"sample in samplesCtrl.samples\">\n" +
    "              <a href=\"#/samples/{{sample}}\">{{sample}}</a>\n" +
    "          </div>\n" +
    "\n" +
    "      </div>\n" +
    "</div>\n"
  );


  $templateCache.put('views/search.html',
    "<cell class=\"col-md-2\"/>\n" +
    "<cell class=\"col-md-8\">\n" +
    "\n" +
    "    <form>\n" +
    "        <input style=\"width: 100%; margin: 3em 0\" id=\"search_text\"\n" +
    "               ng-model=\"searchForm.text\"\n" +
    "               ng-enter=\"submitSearch()\"\n" +
    "               ng-model-options=\"{debounce: 400}\"\n" +
    "               placeholder=\"Enter URL or search terms\">\n" +
    "    </form>\n" +
    "    <div ng-if=\"!searchForm.text\">\n" +
    "\n" +
    "        <div class=\"search_results recent\"\n" +
    "             style=\"display: inline-block; vertical-align: top; margin-right: 3em;\"\n" +
    "             ng-if=\"currentUserHasRole('user')\">\n" +
    "            <h4>My Recent Mosaics</h4>\n" +
    "            <span ng-if=\"myRecentMosaics.length == 0\" style=\"color: #303030\"><i>You have created no mosaics yet.</i></span>\n" +
    "            <div ng-repeat=\"moz in myRecentMosaics\">\n" +
    "                <a ui-sref=\"mosaics.id(moz)\">{{moz['dc:title']}}</a> ({{moz['annotations'].length}})\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"search_results recent\" style=\"display: inline-block\">\n" +
    "            <h4>Recent Mosaics</h4>\n" +
    "            <div ng-repeat=\"moz in recentMosaics\">\n" +
    "                <a ui-sref=\"mosaics.id(moz)\">{{moz['dc:title']}}</a> ({{moz['annotations'].length}})\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"search_results annotation recent\" >\n" +
    "            <h4>Recent Annotations</h4>\n" +
    "            <div style=\"display: table\">\n" +
    "                <annotation-search-result ng-repeat=\"ann in recentAnnotations\" ann=\"ann\"></annotation-search-result>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"search_results mosaics\" ng-if=\"searchForm.text\">\n" +
    "        <h4>Matching Mosaics</h4>\n" +
    "        <div ng-repeat=\"moz in matchingMosaics\">\n" +
    "            <a ui-sref=\"mosaics.id({id:moz.id})\">{{moz['dc:title']}}</a> ({{moz['annotations'].length}})\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"search_results resources\" ng-if=\"searchForm.text\">\n" +
    "        <h4>Matching Resources</h4>\n" +
    "        <div ng-repeat=\"res in matchingResources\">\n" +
    "            <a ui-sref=\"mosaics.id.resource({id:res['moz:inMosaic'],res:res.id})\">{{res['dc:title']}}</a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"search_results annotation\" ng-if=\"searchForm.text\">\n" +
    "        <h4>Matching Annotations</h4>\n" +
    "        <div style=\"display: table\">\n" +
    "            <annotation-search-result ng-repeat=\"ann in matchingAnnotations\" ann=\"ann\"></annotation-search-result>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</cell>\n" +
    "<cell class=\"col-md-2\"/>"
  );


  $templateCache.put('views/searchComboTemplate.html',
    "<div class=\"searchCombo\">\n" +
    "    <span ng-repeat=\"acSource in autocompleteSources\" ng-click=\"setACSource(acSource)\" ng-class=\"{active: (acSource==selectedAcSource)}\">{{acSource.name}}</span>\n" +
    "</div>"
  );


  $templateCache.put('views/search_dropdown.html',
    "<div style=\"position: relative\">\n" +
    "    <div class=\"spinner\"\n" +
    "         style=\"position: absolute;right: 3em;top: 1.5em; display: none\">\n" +
    "        <div class=\"rect1\"></div>\n" +
    "        <div class=\"rect2\"></div>\n" +
    "        <div class=\"rect3\"></div>\n" +
    "    </div>\n" +
    "    <input class=\"dropdownInput form-control\" ng-model=\"modelAttr[displayField]\" placeholder=\"Enter URL or search terms\" type=\"text\"/>\n" +
    "</div>\n"
  );


  $templateCache.put('views/sparql.html',
    "<div >\n" +
    "\n" +
    "    <div style=\"position:relative\">\n" +
    "        <div style=\"float:right; margin-right:1em; margin-top: 1em\">\n" +
    "            <div class=\"btn-group\" style=\"margin-bottom: 10px; width: 100%\">\n" +
    "                <button type=\"button\" class=\"btn btn-primary\">Sample queries</button>\n" +
    "                <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
    "                    <span class=\"caret\"></span>\n" +
    "                    <span class=\"sr-only\">Toggle Dropdown</span>\n" +
    "                </button>\n" +
    "                <ul class=\"dropdown-menu\" style=\"right: 0; left: inherit\">\n" +
    "                    <li ng-repeat=\"(label,url) in sampleQueries\"><a ng-click=\"loadQuery(url)\">{{label}}</a></li>\n" +
    "                </ul>\n" +
    "            </div><br/>\n" +
    "            <button class=\"btn btn-primary\" style=\"margin-bottom: 10px; width: 100%\" ng-click=\"sendQuery()\">query</button><br/>\n" +
    "            <button class=\"btn btn-primary\" style=\"margin-bottom: 10px; width: 100%\" ng-click=\"sendDelete()\" ng-if=\"hasPermission('admin')\">delete</button><br/>\n" +
    "            <button class=\"btn btn-primary\" style=\"margin-bottom: 10px; width: 100%\" ng-click=\"sendPut()\" ng-if=\"hasPermission('admin')\">put</button>\n" +
    "        </div>\n" +
    "\n" +
    "        <ui-codemirror id=\"sparqlEditor\" ui-codemirror-opts=\"editorOptions\" ng-model=\"query\"></ui-codemirror>\n" +
    "    </div>\n" +
    "\n" +
    "    <div style=\"position:relative\">\n" +
    "        <div class=\"btn-group\" style=\"position:absolute; right:1em; top: 1em; z-index: 3\">\n" +
    "            <label class=\"btn btn-primary\" ng-model=\"format\" uib-btn-radio=\"'rdf'\">RDF</label>\n" +
    "            <label class=\"btn btn-primary\" ng-model=\"format\" uib-btn-radio=\"'ttl'\">Turtle</label>\n" +
    "            <label class=\"btn btn-primary\" ng-model=\"format\" uib-btn-radio=\"'jsonld'\">JSON-LD</label>\n" +
    "        </div>\n" +
    "        <ui-codemirror id=\"resultEditor\" ui-codemirror-opts=\"editorOptions\" ng-model=\"result\"></ui-codemirror>\n" +
    "    </div>\n" +
    "</div>\n"
  );

}]);
