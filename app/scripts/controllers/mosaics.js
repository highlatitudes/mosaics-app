Array.prototype.setContent = function(arr) {
    this.length = 0;
    this.push.apply(this, arr);
};

if (!Array.prototype.last){
    Array.prototype.last = function(){
        return this[this.length - 1];
    };
};

var mosaicsControllers = angular.module('mosaicsControllers', [])

// create annotation object (as per FRAGMENTSVIZ model) from annotation data
var getAnnotationObject = function(annotationData) {
    return annotationData && {
        get description() {return this.data['dc:description'] || ""},  //TODO have a real description
        get name() {return this.data['dc:title']},
        get fromUri() {return this.data.target.source},
        get toUri() {return this.data.body && this.data.body.source},
        get toFragment() {return this.data.body && this.data.body.selector && FRAGVIZ.UTILS.parseFragment('#'+this.data.body.selector.value)},
        get fragment() {return this.data.target.selector && FRAGVIZ.UTILS.parseFragment('#'+this.data.target.selector.value)},
        data: annotationData,
        // this is needed for the annotBoxes map to obtain a unique hash
        toString: function() {return annotationData.id}
    }
} ;

mosaicsControllers.directive('ngConfirmClick', [
    function(){
        return {
            priority: -1,
            restrict: 'A',
            link: function(scope, element, attrs){
                element.bind('click', function(e){
                    var message = attrs.ngConfirmClick;
                    // confirm() requires jQuery
                    if(message && !confirm(message)){
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }
                });
            }
        }
    }
]);

/**
 * Controller for the login dialog.
 * @Deprecated
 */
mosaicsControllers.controller('LoginCtrl', ['$scope', '$auth', 'User', 'Authentication', function ($scope, $auth, User, Authentication) {

    $scope.authenticate = function (provider) {
        $auth.authenticate(provider);
    };

    $scope.logout = function (provider) {
        $auth.logout();
    };
}]);




mosaicsControllers.controller('EditAnnotationFilterCtrl',
    ['$scope', '$uibModalInstance', 'filter', 'AppState', '$timeout',
    function ($scope, $uibModalInstance, filter, AppState, $timeout) {

    $scope.filter = filter;

    $scope.ok = function () {
        $uibModalInstance.close($scope.filter);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


    $scope.autocompleteSources = [
        {   name: "Resource",
            url: AppState.apiUrl + '/resources.json',
            resultTransformer: function (response) {
                var results = angular.fromJson(response)
                return {
                    suggestions: $.map(results, function (result) {
                        return { value: result['dc:title'], data: result };
                    })
                };
            },
            setResult: function (suggestion, res) {
                $timeout(function () {
                    res.source = suggestion.data.source
                    if (suggestion.data.selector)
                        res.selector = {
                            type: suggestion.data.selector.type,
                            value: suggestion.data.selector.value
                        }
                })
            }
        }
    ]

    setTimeout(function() {
        $('#linkType')
            .devbridgeAutocomplete({
                triggerSelectOnValidInput : false,
                //minChars: 3,
                appendTo: '#linkTypeContainer',
                showNoSuggestionNotice: true,
                preserveInput: false,
                //TODO fetch vocabularies from server
                //serviceUrl: function(token) {return $scope.selectedAcSource.url},
                lookup: [
                    {value:'hasNationalTransposition', data:'http://inspire.ec.europa.eu/mosaics/hasNationalTransposition'},
                    {value:'hasSubNationalTransposition', data:'http://inspire.ec.europa.eu/mosaics/hasSubNationalTransposition'},
                    {value:'hasImplementationReport', data:'http://inspire.ec.europa.eu/mosaics/hasImplementationReport'},
                    {value:'hasImplementationRule', data:'http://inspire.ec.europa.eu/mosaics/hasImplementationRule'},
                    {value:'hasTechnicalGuideline', data:'http://inspire.ec.europa.eu/mosaics/hasTechnicalGuideline'}
                ],
                paramName: 'query',
                //dataType: 'jsonp',
                noSuggestionNotice: '<i>No Results</i>',
                onSearchStart: function(params) {
                    $(this.parentElement).find(".spinner").show()
                },
                onSearchComplete: function(q, suggestions) {
                    $(this.parentElement).find(".spinner").hide()
                    $("div.autocomplete-suggestions").css({"min-width": $("#annTargetContainer input").outerWidth()});
                },
                onSelect: function(suggestion) {
                    $timeout(function() {
                        ($scope.filter.body || ($scope.filter.body = {}))['moz:linkType'] = suggestion.data
                    })
                }
            })

    }, 300)
}])

/**
 * Directive for 'mosaics' attribute that adds mosaic list management.
 * Required parent for mosaic display and editor directives
 */
mosaicsControllers.directive('mosaics', function() {
    return {
        restrict: 'A',
        controller: 'MosaicsCtrl',
        priority: 100 // make sure 'mosaics' is evaluated before 'mosaic', short of which 'selected' may not be set on time
    }
})

mosaicsControllers.controller('MosaicsCtrl',
    ['$scope', 'Mosaic', '$stateParams', '$uibModal', 'AppState', '$http', '$timeout',
    function($scope, Mosaic, $stateParams, $uibModal, AppState, $http, $timeout) {

    /**
     * Refresh the list of mosaics according to current $scope.query
     */
    $scope.refreshList = function() {
        //TODO fetch a reduced profile for the list of mosaics (e.g. only id and title)

        if (!$scope.query)
        // default: query all
            $scope.mosaics = Mosaic.query()
        else if ($scope.query == 'mine') {
            $scope.mosaics = Mosaic.mine()
        } else {
            throw "Not implemented : SPARQL queries"
        }
    }

    /**
     * Check URL state params for query
     */
    $scope.params = $stateParams
    $scope.$watch("params.query", function(query) {
        // WARN check because watch triggers twice for each change
        // TODO find root cause
        if ($scope.query != query || query === undefined) {
            $scope.query = query
            $scope.refreshList();
        }
    })

    /**
     * Check URL state params for mosaic ID
     */
    $scope.$watch("params.id", function(id) {
        if (id && !($scope.selected && $scope.selected.id == id)) {
            $scope.select(Mosaic.get({id:id}))
            /*
            Mosaic.get(
                {id:id},
                function(mosaic) {
                    $scope.select(mosaic)
            })
            */
        }
    })

    /**
     * Check URL state params for selected Resource
     */
    $scope.$watch("params.res", function(resId) {
        if (resId && $scope.selected && !($scope.selectedRes && $scope.selectedRes.id == resId)) {
            $scope.selected.$promise.then(function(moz) {
                //TODO would be better to have a resource map by id
                for (var idx = 0, length = moz.resources.length; idx < length; idx++)
                    if (moz.resources[idx].id == resId) {
                        $scope.selectResource(moz.resources[idx])
                        break;
                    }
            })

        }
    })

    $scope.selected = undefined
    $scope.selectedRes

    /**
     * Select a mosaic for display
     * @param mosaic
     */
    $scope.select = function(mosaic) {
        if ($scope.selected != mosaic)
            $scope.selectedRes = undefined
        $scope.selected = mosaic

    }

    /**
     * Is given mosaic the one being displayed
     * @param mosaic
     */
    $scope.isSelected = function(mosaic) {
        return mosaic === $scope.selected
    }

    /**
     * Select a resource for display
     * @param mosaic
     */
    $scope.selectResource = function(res) {
        //TODO check whether given resource is part of currently displayed mosaic ?
        $scope.selectedRes = res
    }

    // TODO issue warning if $dirty and no permission to save

    /**
     * Create a new mosaic instance and select it
     */
    $scope.createMosaic = function() {
        var newMosaic = {
            "type" : "moz:Mosaic",
            "dc:title" : "New Mosaic",
            "$dirty" : "true"
        }

        $scope.editMosaicModal(
            newMosaic,
            function(updatedMoz) {
                $scope.saveMosaic(updatedMoz)

                $scope.select(updatedMoz)
                $scope.refreshList()
            }
        )


    }



    $scope.editResourceModal = function (res, callback) {
        callback = callback || function(updatedRes) {
            angular.copy(updatedRes, res)
        }

        var modalInstance = $uibModal.open({
            templateUrl: 'views/editResource.html',
            controller: 'ResourceEditCtrl',
            resolve: {
                resource: function () {
                    return angular.copy(res);
                }
            }
        }).result.then(callback)
    }

    $scope.editMosaicModal = function (moz, callback) {
        callback = callback || function(updatedMosaic) {
            angular.copy(updatedMosaic, res)
        }

        var modalInstance = $uibModal.open({
            templateUrl: 'views/editMosaic.html',
            controller: 'MosaicEditCtrl',
            resolve: {
                mosaic: function () {
                    return angular.copy(moz);
                }
            }
        }).result.then(callback)
    }

    /**
     * Create a Resource instance.
     * Triggers a modal dialog, then adds the new resource to current mosaic and saves it.
     * @param moz
     */
    $scope.createResource = function(moz) {

        var newResource = {
            "type" : "moz:Resource",
            "dc:title" : "New Resource"
        }

        $scope.editResourceModal(
            newResource,
            function(updatedResource) {
                if (!moz.resources) moz.resources = []
                moz.resources.push(updatedResource)
                $scope.saveMosaic(moz)

                $scope.refreshList()
            }
        )

    }

    /**
     * Save given mosaic
     * @param moz
     * @param callback
     */
    $scope.saveMosaic = function(moz, callback) {
        Mosaic.save({}, moz, function(obj) {
            moz.$dirty = false
            $scope.refreshList()
            callback && callback(obj)
        })
    }

    /**
     * Delete given mosaic
     * @param moz
     */
    $scope.deleteMosaic = function(moz) {
        moz.$delete(function() {
            // TODO better to simply remove from list, rather than whole refresh ?
            $scope.refreshList()
        })
    }

    /**
     * Delete given resource, if found in currently selected mosaic.
     * @param moz
     */
    $scope.deleteResource = function(moz, res) {
        if (moz.resources) {
            var index = moz.resources && moz.resources.indexOf(res);
            if (index > -1) {
                moz.resources.splice(index, 1);
            }
        }
    }

    //TODO merge this with deleteAnnotation in mosaic scope
    $scope.deleteAnnotationFromMosaic = function(moz, annot) {
        if (moz.annotations) {
            var index = moz.annotations && moz.annotations.indexOf(annot);
            if (index > -1) {
                moz.annotations.splice(index, 1);
            }
        }
    }

}]);


mosaicsControllers.directive('mosaic', function() {
    return {
        restrict: 'A',
        controller: 'MosaicCtrl',
        require: '?^overlaidAnnotations',
        link: function(scope, element, attrs, overlaidAnnotationsCtrl) {
            scope.overlaidAnnotationsCtrl = overlaidAnnotationsCtrl;

            scope.filteredAnnotations = overlaidAnnotationsCtrl && scope.overlaidAnnotationsCtrl.annotations
        }
    }
})

/**
 * Expects a scope variable 'mosaic'
 */
mosaicsControllers.controller('MosaicCtrl',
    ['$scope', 'Mosaic', '$attrs', '$uibModal', '$http', 'AppState', '$stateParams', '$timeout',
    function($scope, Mosaic, $attrs, $uibModal, $http, AppState, $stateParams, $timeout) {

    var mosaicVarName = $attrs['mosaicVar'] || 'mosaic'

    $scope.getMosaic = function() {
        return $scope[mosaicVarName]
    }


    $scope.params = $stateParams
    /**
     * Check URL state params for selected annotation
     */
    $scope.$watch("params.ann", function(annId) {
        if (annId && $scope.selected) {
            $scope.selected.$promise.then(function(moz) {

                var selectedAnn = _.find($scope.selected.annotations, function(ann) {
                    return ann.id == annId
                })

                $scope.selectedRes =
                    _.find(moz.resources, function(res) {return res.source == selectedAnn.target.source}) ||
                    _.find(moz.resources, function(res) {return res.source == selectedAnn.body.source}) ||
                    selectedAnn.target

                $timeout(function() { // make sure the selectedRes is digested before triggering the annotation event
                    $scope.gotoAnnotation(selectedAnn, $scope.params['open'] && true)
                })

            })

        }
    })

    /**
     * Focus on given annotation.
     * Fires an 'annotation-focus' event for given annotation; all listening components
     * should act upon it adequately.
     * @param annot
     */
    $scope.gotoAnnotation = function(annot, openAnnot) {
        $scope.$broadcast ('annotation-focus', annot, openAnnot);
    }

    $scope.highlightAnnotation = function(annot, toggle) {
        $scope.$broadcast ('annotation-highlight', annot, toggle);
    }

    /*
        Annotations filtering
     */
    $scope.editAnnotationFilterModal = function (targetUri) {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/annotationFilter.html',
            controller: 'EditAnnotationFilterCtrl',
            resolve: {
                filter: function () {
                    var filter = JSON.parse(JSON.stringify($scope.overlaidAnnotationsCtrl.getFilter())) || {}

                    if (targetUri) {
                        if (!filter.target)
                            filter.target = {}
                        if (!filter.target.source)
                            filter.target.source = targetUri
                    }

                    return filter;
                }
            }
        }).result.then(function(filter) {
                $scope.overlaidAnnotationsCtrl.setFilter(filter)
            })
    }

    $scope.$watch(mosaicVarName, function(newValue, oldValue) {
        if (newValue && oldValue && (!oldValue.$promise || oldValue.$resolved))
            newValue.$dirty = true
    }, true)

    $scope.resources = []
    $scope.annotations = []
    $scope.annotationsFromMap = {}
    $scope.annotationsToMap = {}

    $scope.editMode = false
    $scope.toggleEdit = function() {
        $scope.editMode = !$scope.editMode
    }

    $scope.editorOptions = {
        lineWrapping : true,
        lineNumbers: true,
        //readOnly: 'nocursor',
        mode: 'application/ld+json',
        matchBrackets: true
    }

    $scope.$watch("jsonMosaic", function(newValue) {
        if ($scope.editMode && $scope.getMosaic() && (!$scope.getMosaic().$promise || $scope.getMosaic().$resolved)) {
            //WARN does this properly remove attributes ?
            angular.extend($scope.getMosaic(), angular.fromJson($scope.jsonMosaic))
        }
    })


    $scope.addAnnotation = function(annotation) {

        // send new annotation event up the stack
        $scope.$emit("hilats.add_annotation", annotation)

        $scope.getMosaic().annotations = $scope.getMosaic().annotations || []
        $scope.getMosaic().annotations.push(annotation)
    }

    $scope.deleteAnnotation = function(annotation) {
        if ($scope.getMosaic().annotations) {
            var idx = _.findIndex($scope.getMosaic().annotations, function(ann) {
                return ann == annotation ||   // newly created annotations have no id
                       ann["id"] == annotation["id"]})
            if (idx > -1) {
                $scope.getMosaic().annotations.splice(idx, 1);
            }
        }
    }

    $scope.$watch("getMosaic()", function() {

        if ($scope.getMosaic() && (!$scope.getMosaic().$promise || $scope.getMosaic().$resolved)) {
            $scope.jsonMosaic = angular.toJson($scope.getMosaic(), true)

            $scope.resources = $scope.getMosaic().resources || []

            // collate all annotations from resource and from main annotations list
            $scope.annotations = ($scope.getMosaic().annotations && $scope.getMosaic().annotations.slice(0)) || []
            angular.forEach($scope.resources, function(res) {
                angular.forEach(res['annotatedBy'], function(annot) {
                    annot['target'] = annot['target'] || {'source' : res['source']}
                    $scope.annotations.push(annot)
                })
            })

            $scope.annotationsFromMap = {}
            $scope.annotationsToMap = {}
            angular.forEach($scope.annotations, function(annot) {
                var fromMap = annot.target && annot.target.source && ($scope.annotationsFromMap[annot.target.source] || ($scope.annotationsFromMap[annot.target.source] = []))
                fromMap.push(annot)

                var toMap = annot.body && annot.body.source && ($scope.annotationsToMap[annot.body.source] || ($scope.annotationsToMap[annot.body.source] = []))
                toMap && toMap.push(annot)

                /* WARN This causes object identity to change, and may prevent object equality on newly created annotations */
                if (!annot.id)
                    Object.defineProperty(annot, "id",
                        {get: function() {
                            return (this.target.selector && this.target.selector.value) + (this.body && this.body.selector && this.body.selector.value)
                        }})

            })



        }
    }, true /* watch in depth, at least to detect promise resolution */)
}]);

/**
 * Attribute directive to wrap mosaic display layout
 */
mosaicsControllers.directive('mosaicDisplay', function() {
    return {
        restrict: 'A',
        scope: {
            displayedResource: '=ngRes',
            annotationsFromMap: '=',
            annotationsToMap: '=',
            filteredAnnotations: '=', //TODO should this be here, or in another scope ?
            filterAnnotationsDialog: '=' //// copy annotation filtering dialog fn into 'filterAnnotations' for displayActionBar.html
        },
        controller: 'MosaicDisplayCtrl',
        controllerAs: 'mosaicDisplayCtrl',
        transclude: true,
        compile: function (tElement, attrs, transclude) {
            return function ($scope) {
                transclude($scope, function (clone) {
                    tElement.append(clone);
                });
            };
        }
    }
})
mosaicsControllers.controller('MosaicDisplayCtrl',
    ['$scope', '$timeout', '$uibModal', '$compile', '$q', '$templateRequest', '$rootScope',
    function($scope, $timeout, $uibModal, $compile, $q, $templateRequest, $rootScope) {
    $scope.displayedAnnotations = {}

    // map of side-displayed annotations. (key,value) = (annHash, viewer)
    $scope.displayedAnnotationResources = {}
    $scope.capturedFragment = undefined
    $scope.loading = false;

    $scope.editAnnotationModal = function (ann, callback) {
        var modalInstance = $uibModal.open({
            templateUrl: 'views/editAnnotation.html',
            controller: 'AnnotationEditCtrl',
            resolve: {
                annotation: function () {
                    return angular.copy(ann);
                }
            }
        }).result.then(callback)

        /*
         setTimeout(function() {
         //TODO hack to force display of modal - find fix
         $(".modal").toggleClass("fade", false)
         }, 500)
         */
    }

    $scope.editAnnotationOnClick = function ($event) {
        //TODO is climbing up the DOM to .annot the best way to get the srcAnnot ?
        var editedAnn = $($event.currentTarget).parents(".annot")[0].srcAnnot.data
        $scope.editAnnotationModal(
            editedAnn,
            function(updatedAnnotation) {
                angular.copy(updatedAnnotation, editedAnn)
            }
        )
        $event.stopPropagation();
    }

    $scope.deleteAnnotationOnClick = function ($event) {
        $scope.$parent.deleteAnnotation($($event.currentTarget).parents(".annot")[0].srcAnnot.data)
        $event.stopPropagation();
    }

    // Define custom AnnotationRenderer to add hover, click actions
    $scope.annotationRenderer = new FRAGVIZ.VIEWERS.AnnotationRenderer()


    /* Return (set of) HTML elems to be appended to the action bar container */
    $scope.annotationRenderer.renderActionBarContent = function(annotation, viewer) {
        var wrapperDiv = $("<div></div>")
        $templateRequest("views/annotationActions.html").then(function(html){
            var annScope = $scope.$new();
            annScope.hasPermission = $rootScope.hasPermission;
            annScope.annotation = annotation;
            var annotationOverlay = angular.element(html);
            $compile(annotationOverlay)(annScope);
            wrapperDiv.append(annotationOverlay)
        });

        wrapperDiv[0].srcAnnot = annotation

        return wrapperDiv
    }

    /* Return (set of) HTML elems to be appended to the details container */
    $scope.annotationRenderer.renderDetailsContent = function(annotation, viewer) {
        var wrapperDiv = $("<div></div>")
        $templateRequest("views/annotationDetails.html").then(function(html){
            var annScope = $scope.$new();
            annScope.annotation = annotation;
            annScope.linkTypeName = annotation.data.body &&
                annotation.data.body['moz:linkType'] &&
                annotation.data.body['moz:linkType'].split('/').slice(-1)[0]

            var annotationOverlay = angular.element(html);
            $compile(annotationOverlay)(annScope);
            wrapperDiv.append(annotationOverlay)
        });

        return wrapperDiv
    }

    /* returns a listener function that takes the event as arg */
    $scope.annotationRenderer.createClickListener = function(annotation, viewer) {
        // would be better to get viewer from annotBox somehow
        return function(e) {
            viewer.clearCapturedFragment();
            if (e.ctrlKey || e.metaKey) {
                $scope.$apply(function () {
                    $scope.editAnnotationModal(annotation.data,
                        function(updatedAnnotation) {
                            angular.copy(updatedAnnotation, editedAnn)
                        })
                })
            } else {
                $scope.$apply(function () {
                    $scope.displayAnnotationResource(annotation, viewer)
                })
            }
            return false
        }
    }

    /* returns a listener function that takes (bool hoverIn, event) as args */
    $scope.annotationRenderer.createHoverListener = function(annotation, viewer) {
        return function (hoverIn, event) {
            if (annotation.data.body) {
                $('[data-vieweruri="' + annotation.data.body.source + '"]')
                    .each(function () {
                        $(this).prop('curViewer').toggleAnnotationHighlight(annotation.data.id, hoverIn)
                    })
                    .parent().toggleClass("active", hoverIn)
            }
        }
    }



    $scope.annotationRenderer.renderCapturedFragment = function(fragment, $elem, viewer) {

        $templateRequest("views/captureActionBar.html").then(function(html){
            var captureScope = $scope.$new();
            captureScope.fragment = fragment;
            captureScope.capture = _.bind(viewer.onFragmentCapture, viewer)

            var actionBar = angular.element(html);
            $compile(actionBar)(captureScope);
            $elem.append(actionBar)
        });

    }

    $scope.captureFragmentFn = function(fragment, callback) {
        var candidateAnnotation = {
            'target' : {
                source: $scope.displayedResource['source'],
                selector: {
                    type: 'FragmentSelector',
                    value: FRAGVIZ.UTILS.encodeHash(fragment.hash)
                }},
            'type': 'Annotation'
        }

        $scope.editAnnotationModal(candidateAnnotation, function(result) {
            //TODO add annotation to current resource rather than global scope
            $scope.$parent.addAnnotation(result)
            callback && callback(result)
        })
    }

    this.displayAnnotationResource = $scope.displayAnnotationResource = function(ann, viewer) {

        var directedPair = viewer.getDirectedPair(ann)

        if (directedPair.linked && directedPair.linked.url) {

            //TODO change display location depending on directedPair.isTo flag
            //TODO currently displayedAnnotationResources assumes the 'to' url must be displayed
            //     see warn below
            var existingViewerEl = $('[data-vieweruri="'+directedPair.linked.url+'"]')
            if (existingViewerEl.length > 0) {
                // resource is already displayed ; simply re-focus
                if (directedPair.linked.fragment) {
                    existingViewerEl.each(function () {
                        $(this).prop('curViewer').gotoFragment(directedPair.linked.fragment)
                    })
                }
            } else {
                if (!directedPair.isTo)
                    $scope.displayedAnnotationResources[ann] = ann
                else
                    console.warn("Display of 'to' annotations not supported")
            }
        }
    }

    $scope.closeAnnotationResource = function(ann) {
        $timeout(function() {
            // put in timeout to force object change in next angular cycle
            delete $scope.displayedAnnotationResources[ann]
        })
    }
} ] ) ;




mosaicsControllers.controller('MosaicEditCtrl',
    ['$scope', '$uibModalInstance', 'mosaic', 'AppState', '$timeout', '$compile', '$templateRequest', '$rootScope',
    function ($scope, $uibModalInstance, mosaic, AppState, $timeout, $compile, $templateRequest, $rootScope) {
    $scope.editedMoz= mosaic;


    $scope.ok = function () {
        $uibModalInstance.close($scope.editedMoz);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}])




mosaicsControllers.controller('ResourceEditCtrl',
    ['$scope', '$uibModalInstance', 'resource', 'AppState', '$timeout', '$compile', '$templateRequest', '$rootScope',
    function ($scope, $uibModalInstance, resource, AppState, $timeout, $compile, $templateRequest, $rootScope) {

    $scope.autocompleteSources = $rootScope.catalogSources

    $scope.editedRes= resource;
    $scope.jsonEditedRes = ' ';

    $scope.editorOptions = {
        lineWrapping : true,
        lineNumbers: true,
        //readOnly: 'nocursor',
        mode: 'application/ld+json',
        matchBrackets: true
    }

    $scope.$watch('editedRes', function(newVal) {
        $scope.jsonEditedRes = angular.toJson(newVal , true);
    })

    $scope.$watch("jsonEditedRes", function(newValue) {
        if ($scope.editMode && $scope.editedRes) {
            //WARN does this properly remove attributes ?
            angular.extend($scope.editedRes, angular.fromJson($scope.jsonEditedRes))
        }
    })

    $scope.ok = function (projectId, type) {
        $uibModalInstance.close($scope.editedRes);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.preview = function(res) {
        $scope.originalPreviewedRes = res
        $scope.previewedRes = angular.copy(res)
    }

    $scope.captureFragment = function(fragment) {
        $timeout(function() {
            $scope.originalPreviewedRes.selector = {
                type: 'FragmentSelector',
                value: FRAGVIZ.UTILS.encodeHash(fragment.hash)
            }
        })
    }

}])



mosaicsControllers.controller('AnnotationEditCtrl',
    ['$scope', '$uibModalInstance', 'annotation', 'AppState', '$timeout', '$compile', '$templateRequest', '$rootScope',
    function ($scope, $uibModalInstance, annotation, AppState, $timeout, $compile, $templateRequest, $rootScope) {

    $scope.autocompleteSources = $rootScope.catalogSources

    $scope.editedAnn= annotation;
    $scope.jsonEditedAnn = ' ';

    $scope.editorOptions = {
        lineWrapping : true,
        lineNumbers: true,
        //readOnly: 'nocursor',
        mode: 'application/ld+json',
        matchBrackets: true
    }

    $scope.$watch('editedAnn', function(newVal) {
        $scope.jsonEditedAnn = angular.toJson(newVal , true);
    })

    $scope.$watch("jsonEditedAnn", function(newValue) {
        if ($scope.editMode && $scope.editedAnn) {
            //WARN does this properly remove attributes ?
            angular.extend($scope.editedAnn, angular.fromJson($scope.jsonEditedAnn))
        }
    })

    $scope.ok = function (projectId, type) {
        $uibModalInstance.close($scope.editedAnn);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.preview = function(res) {
        $scope.originalPreviewedRes = res
        $scope.previewedRes = angular.copy(res)
    }

    $scope.switch = function() {
        var tmp = $scope.editedAnn.body
        $scope.editedAnn.body = $scope.editedAnn.target
        $scope.editedAnn.target = tmp
    }

    $scope.captureFragment = function(fragment) {
        $timeout(function() {
            $scope.originalPreviewedRes.selector = {
                type: 'FragmentSelector',
                value: FRAGVIZ.UTILS.encodeHash(fragment.hash)
            }
        })
    }

    setTimeout(function() {
        $('#linkType')
            .devbridgeAutocomplete({
                triggerSelectOnValidInput : false,
                //minChars: 3,
                appendTo: '#linkTypeContainer',
                showNoSuggestionNotice: true,
                preserveInput: false,
                //TODO fetch vocabularies from server
                //serviceUrl: function(token) {return $scope.selectedAcSource.url},
                lookup: [
                    {value:'hasNationalTransposition', data:'http://inspire.ec.europa.eu/mosaics/hasNationalTransposition'},
                    {value:'hasSubNationalTransposition', data:'http://inspire.ec.europa.eu/mosaics/hasSubNationalTransposition'},
                    {value:'hasImplementationReport', data:'http://inspire.ec.europa.eu/mosaics/hasImplementationReport'},
                    {value:'hasImplementationRule', data:'http://inspire.ec.europa.eu/mosaics/hasImplementationRule'},
                    {value:'hasTechnicalGuideline', data:'http://inspire.ec.europa.eu/mosaics/hasTechnicalGuideline'}
                ],
                paramName: 'query',
                //dataType: 'jsonp',
                noSuggestionNotice: '<i>No Results</i>',
                onSearchStart: function(params) {
                    $(this.parentElement).find(".spinner").show()
                },
                onSearchComplete: function(q, suggestions) {
                    $(this.parentElement).find(".spinner").hide()
                    $("div.autocomplete-suggestions").css({"min-width": $("#linkType").outerWidth()});
                },
                onSelect: function(suggestion) {
                    $timeout(function() {
                        $scope.editedAnn.body['moz:linkType'] = suggestion.data
                    })
                }
            })
    }, 300)
}])



/**
 * A directive that embeds resource-related components. If present, embedded components should set relevant attribtues such as curViewer.
 */
mosaicsControllers.directive('resourceContext', function() {
    return {
        restrict: 'A',
        controller: function($scope) {
            this.setCurrentViewer = function(viewer) {
                $scope.curViewer = viewer
            }
        }
    }
})


mosaicsControllers
    .directive('overlaidAnnotations', function() {
        return {
            restrict: 'A',
            controller: 'OverlaidAnnotationsCtrl',
            link: function (scope, element, attrs) {
                var filter = scope.$eval(attrs.overlaidAnnotations)
                if (filter)
                    scope.filter = filter
            }
        }
    })
    .controller('OverlaidAnnotationsCtrl',
    ['$scope', '$http', 'AppState', '$timeout', '$stateParams', 'Annotation', '$q',
    function($scope, $http, AppState, $timeout, $stateParams, Annotation, $q) {
        this.extraAnnotations = []
        this.filteredAnnotations = []
        this.annotations = []

        $scope.getFilter = this.getFilter = function() {
            return $scope.filter;
        };

        this.setFilter = function(filter) {
            $scope.filter = filter;
        };

        this.getFilteredAnnotations = function() {
            return $scope.filteredAnnotations;
        };

        var _this = this;

        this.updateAnnotations = function() {
            var ids = {};
            _this.annotations.length = 0;
            _this.filteredAnnotations.concat(_this.extraAnnotations).forEach(function(annot) {
                if (!annot.id || !ids[annot.id] ) {
                    annot.id && (ids[annot.id] = annot);
                    _this.annotations.push(annot);
                }
            })
        }

        this.filterAnnotations = function(filter) {
            //TODO define a service
            $http.post(AppState.apiUrl+'/annotations/search.json', filter)
                .then(function(res) {
                    $timeout(function() {
                        _this.filteredAnnotations.setContent(res.data)
                        _this.updateAnnotations()
                    })
                })
        }

        $scope.$watch("filter", function() {
            if ($scope.filter) {
                _this.filterAnnotations($scope.filter);
            }
        }) // no in-depth watch, as filter should be set to a new instance on change, not updated

        if ($stateParams['ann']) {

            var selectedAnn = _.find(_this.annotations, function(ann) {
                return ann.id == $stateParams['ann']
            })
            var deferredAnnotation = $q.defer()
            if (selectedAnn) {
                deferredAnnotation.resolve(selectedAnn)
            } else {
                Annotation.get({id: $stateParams['ann']}).$promise.then(
                    function(annot) {
                        _this.extraAnnotations.push(annot);
                        _this.updateAnnotations();

                        deferredAnnotation.resolve(annot)
                    })
            }

            deferredAnnotation.promise.then(function(annot) {
                $scope.gotoAnnotation(annot)
            })

        }
    }])

/**
 * Attribute directive to wrap mosaic display layout
 */
mosaicsControllers.directive('resourceDisplay', function() {
    return {
        restrict: 'A',
        scope: {
            mosaicDisplayCtrl: '=',
            displayedResource: '=ngRes',
            annotationsFromMap: '=',
            allowGraphDisplay: '=',
            annotationsToMap: '=',
            closeViewerFn: '=',
            captureFragmentFn: '=',
            annotationClickFn: '=',
            annotationRenderer: '=',
            filteredAnnotations: '=', //TODO should this be here, or in another scope ?
            filterAnnotationsDialog: '=' //// copy annotation filtering dialog fn into 'filterAnnotations' for displayActionBar.html
        },

        // look for a parent resource-context directive. If any, set the resCtxCtrl
        require: '?^^resourceContext',
        link: function(scope, element, attrs, resCtxCtrl) {
            scope.resCtxCtrl = resCtxCtrl
        },
        template: '<div ng-show="!displayGraph" class="resourceDisplayContainer" style="height: 100%;"></div>' +
                  '<div class="graphViewer" ng-show="displayGraph" style="height: 100%;"><div class="hoverMenu widget"><i class="glyphicon-th-large glyphicon" ng-click="toggleGraphDisplay(false)"></i></div>' +
                  '<div class="graphContainer"  style="height: 100%;"></div></div>',

        controller: 'ResourceDisplayCtrl',
        transclude: true
    }
})


mosaicsControllers.controller('ResourceDisplayCtrl',
    ['$scope', '$element', '$timeout', '$q', '$templateRequest', '$compile',
    function($scope, $element, $timeout, $q, $templateRequest, $compile) {
    $scope.loading = false;

    var deferredCurViewer; // leave null at first, will be initialized in displayMainResource
    var displayedAnnotationsWatcher;

    // Listen on 'annotation-focus' ; focus displayed resource when it is the subject of the annotation
    $scope.$on('annotation-focus', function(e, annotationData, openAnnot) {
        deferredCurViewer.promise.then(function(curViewer) {
            // calling getAnnotationObject re-creates a Fragviz annotation, while it should exist already in displayedAnnotations
            var annotation = getAnnotationObject(annotationData)
            var directedPair = annotation && curViewer.getDirectedPair(annotation)
            if (directedPair && directedPair.fragment)
                curViewer && curViewer.onReady(function () {
                    curViewer.gotoFragment(directedPair.fragment)
                    if (openAnnot && $scope.mosaicDisplayCtrl)
                        $scope.mosaicDisplayCtrl.displayAnnotationResource(annotation, curViewer)
                })
        })
    });

    $scope.$on('annotation-highlight', function(e, annotationData, toggle) {
        deferredCurViewer.promise.then(function(curViewer) {

            curViewer && curViewer.onReady(function () {
                curViewer.toggleAnnotationHighlight(annotationData.id, toggle)
            })
        })
    });

    $scope.annotationsDisplayEnabled = true
    $scope.toggleAnnotations = function() {
        $scope.annotationsDisplayEnabled = !$scope.annotationsDisplayEnabled
    }

    $scope.toggleGraphDisplay = function(flag) {
        if (flag === undefined)
            $scope.displayGraph = !$scope.displayGraph
        else
            $scope.displayGraph = flag

        if ($scope.displayGraph) {
            //TODO move graph logic into directive
            var nodesByUri = {}
            var nodes = []

            var mainNode =
            {data: {
                id: $scope.displayedResource.source,
                uri: $scope.displayedResource.source,
                label : $scope.displayedResource['dc:title'] || $scope.displayedResource.id,
                //TODO parent: get mosaic id here
            }
            }
            nodes.push(mainNode)
            nodesByUri[mainNode.data.uri] = mainNode

            /*
            angular.forEach($scope.$parent.getMosaic().resources, function(res) {
                var node =
                {data: {
                    id: res.source,
                    uri: res.source,
                    label : res['dc:title'] || res.id,
                    //TODO parent: get mosaic id here
                }
                }
                nodes.push(node)
                nodesByUri[node.data.uri] = node
            })
            */
            var edges = []
            // short of proper Object.values impl, use this
            angular.forEach($scope.displayedAnnotations, function(ann) {
                var edge =
                {data:
                {id: ann.toString(),
                    source:ann.fromUri,
                    target:ann.toUri,
                    label: ann.data['dc:title'] || ann.data['dc:title']}
                }
                if (! nodesByUri[edge.data.source]) {
                    var node = {data: {
                        id: edge.data.source,
                        label: edge.data.source
                    }}
                    nodes.push(node)
                    nodesByUri[edge.data.source] = node
                }
                if (! nodesByUri[edge.data.target]) {
                    var node = {data: {
                        id: edge.data.target,
                        label: edge.data.target
                    }}
                    nodes.push(node)
                    nodesByUri[edge.data.target] = node
                }
                edges.push(edge)
            })

            $timeout(function() {
                $scope.currentGraph = cytoscape({

                    container: $element.find(".graphContainer"),

                    elements: nodes.concat(edges),

                    style: [ // the stylesheet for the graph
                        {
                            selector: 'node',
                            style: {
                                'background-color': '#666',
                                'label': 'data(label)'
                            }
                        },
                        {
                            selector: 'edge',
                            style: {
                                'width': 3,
                                'line-color': '#ccc',
                                'target-arrow-color': '#ccc',
                                'target-arrow-shape': 'triangle'
                            }
                        }
                    ]

                });

                $scope.currentGraph.layout({
                    name: 'cose',

                    // Constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
                    boundingBox: {x1: 0,y1:0,w:$scope.currentGraph.container().offsetWidth,h:$scope.currentGraph.container().offsetHeight},
                })
            })
        }
    }

    $scope.displayResource = function(fvRes) {
        //TODO solve duality between mosaics Resource and Fragviz Annotation/Resource models
        var res = fvRes && (fvRes.data || fvRes)
        if (res) {
            var url, mimeType, focusFragment
            if (res['source']) {
                url = res['source']
                mimeType = res['format']
                focusFragment = res['selector'] && FRAGVIZ.UTILS.parseFragment('#' + res['selector']['value'])
            } else if (res['body']) {
                // assume this is an annotation, take body as resource
                url = res['body']['source']
                mimeType = res['body']['format']
                focusFragment = res['body']['selector'] && FRAGVIZ.UTILS.parseFragment('#' + res['body']['selector']['value'])
            } else {
                //ignore
                return
            }

            $scope.loading = true;

            deferredCurViewer = $q.defer()
            deferredCurViewer.promise.then(function(curViewer) {
                $scope.curViewer = curViewer

                // if a parent resCtxCtrl was found, set the curViewer
                $scope.resCtxCtrl && $scope.resCtxCtrl.setCurrentViewer(curViewer)
            })

            $scope.displayedAnnotationResources = {}

            var displayContainer = $element.find('.resourceDisplayContainer')

            // force the undisplay of current ressource
            // deactivated : underlying viewer should be able to detect whether new resource is different
            //FRAGVIZ.VIEWERS.displayResource(displayContainer, undefined)

            $scope.currentResource = res
            $scope.displayedAnnotations = {}

            $timeout(function() {  // display in a timeout to force it into the next digest cycle
                                   // short of which some viewers (e.g. OL) fail to guess the right div size
                                   // when it is the first resource and the linkedResources block is still hidden
                // if there's a watcher already, disable it while the viewer initializes
                 displayedAnnotationsWatcher && displayedAnnotationsWatcher();
                FRAGVIZ.VIEWERS.displayResource(
                    displayContainer,
                    FRAGVIZ.RESOURCE.FROMURI(url, mimeType),
                    {
                        renderHoverMenu : function($hoverMenu) {
                            this.__proto__.renderHoverMenu.apply(this, arguments);

                         $templateRequest("views/displayActionBar.html").then(function(html){

                             var actionBar = angular.element(html);
                             $compile(actionBar)($scope);
                             $hoverMenu.prepend(actionBar)

                             if ($scope.closeViewerFn)
                                $hoverMenu.prepend(
                                    $("<i class='glyphicon-remove glyphicon'></i>")
                                        .click(function(){
                                            $scope.closeViewerFn(fvRes)
                                        }))

                         });

                        }
                    }
                    )
                    .done(function (viewer) {
                        deferredCurViewer.resolve(viewer)

                        // override default annotationRenderer
                        if ($scope.annotationRenderer)
                            viewer.annotationRenderer = $scope.annotationRenderer

                        var updateDisplayedAnnotations = function() {
                            $scope.displayedAnnotations = {}
                            $scope.annotationsFromMap && angular.forEach($scope.annotationsFromMap[url], function(annot) {
                                var ann = getAnnotationObject(annot)
                                $scope.displayedAnnotations[ann] = ann
                            })
                            $scope.annotationsToMap && angular.forEach($scope.annotationsToMap[url], function(annot) {
                                var ann = getAnnotationObject(annot)
                                $scope.displayedAnnotations[ann] = ann
                            })
                            $scope.filteredAnnotations && angular.forEach($scope.filteredAnnotations, function(annot) {
                                var ann = getAnnotationObject(annot)
                                $scope.displayedAnnotations[ann] = ann
                            })
                        }

                        $scope.$watchCollection("annotationsFromMap['"+url+"']", updateDisplayedAnnotations);
                        $scope.$watchCollection("annotationsToMap['"+url+"']", updateDisplayedAnnotations);
                        $scope.$watchCollection("filteredAnnotations", updateDisplayedAnnotations);


                        viewer.onReady(function (viewer) {
                            $scope.loading = false;

                            $timeout(function() {

                                if (focusFragment) {
                                    viewer.gotoFragment(focusFragment)
                                }

                                if ($scope.captureFragmentFn) {
                                    viewer.allowFragmentCapture($scope.captureFragmentFn)
                                }

                                displayedAnnotationsWatcher = $scope.$watch("[annotationsDisplayEnabled, displayedAnnotations]", function () {

                                    viewer.clearAnnotationDisplay()

                                    if ($scope.annotationsDisplayEnabled) {
                                        // display all annotations
                                        angular.forEach($scope.displayedAnnotations, function (ann, hash) {
                                            viewer.displayAnnotation(ann)
                                        })

                                        /* TODO what is this ?
                                        if ($scope.annotationClickFn)
                                            angular.forEach(viewer.annotBoxes, function (annotBox, annHash) {
                                                $(annotBox).click(function (e) {
                                                    $scope.$apply(function () {
                                                        $scope.annotationClickFn($scope.displayedAnnotations[annHash])
                                                    })
                                                })
                                            })
                                            */
                                        viewer.refreshAnnotations()
                                    }
                                }, true)  // using deep watch to reflect any annotation edit. Performance ?
                            })
                        })

                        })
                    }
                )
        }
    }

    $scope.$watch("displayedResource", $scope.displayResource, true)
}]);




/** ****************************************
 *   Deprecated code, used only in samples
 *  **************************************** */

/**
 * Controller for demo page
 */
mosaicsControllers.controller('DemoCtrl', ['$scope', 'Mosaic', function($scope, Mosaic) {
    $scope.demoMosaic = Mosaic.fromUrl("/samples/demo.json")

}]);

/**
 * App-wide singleton that holds the list of mosaics and selected.
 * @Deprecated
 */
mosaicsControllers.service('MosaicsContext', ['Mosaic', function(Mosaic) {

    this.selected = undefined

    this.refreshList = function() {
        this.mosaics = Mosaic.query()
    }

    this.refreshList();
}]);

mosaicsControllers.controller('MosaicListCtrl', ['$scope', 'MosaicsContext', function($scope, MosaicsContext) {

    this.context = MosaicsContext
    var $this = this
    this.select = function(mosaic) {
        $this.context.selected = mosaic
    }
    this.isSelected = function(mosaic) {
        return mosaic === $this.context.selected
    }

}]);

mosaicsControllers.controller('MosaicDetailCtrl', ['$scope', '$stateParams', 'MosaicsContext', function($scope, $stateParams, MosaicsContext) {
    $scope.phone = Mosaic.get({mosaicId: $stateParams.mosaicId}, function(mosaic) {

    });

    $scope.setImage = function(imageUrl) {

    }
}]);

mosaicsControllers.controller('MosaicEditorCtrl', ['$scope', 'MosaicsContext', 'Mosaic', function($scope, MosaicsContext, Mosaic) {
    $scope.jsonMosaic = ''
    $scope.context = MosaicsContext

    $scope.editorOptions = {
        lineWrapping : true,
        lineNumbers: true,
        //readOnly: 'nocursor',
        mode: 'application/ld+json',
        matchBrackets: true
    }

    $scope.$watch('context.selected', function(newVal) {
        $scope.jsonMosaic = angular.toJson(newVal, true)
    })

    this.saveMosaic = function() {
        $scope.context.selected = new Mosaic(angular.fromJson($scope.jsonMosaic))
        $scope.context.selected.$save(function() {
            $scope.context.refreshList()
        })


    }

}]);

mosaicsControllers.controller('SamplesCtrl', function() {

    this.samples = [
        "mosaicsList",
        "mosaicsViewer",
        "demo"
    ]
});