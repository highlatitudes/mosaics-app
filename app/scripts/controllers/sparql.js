'use strict';

/**
 * @ngdoc function
 * @name mosaicsAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the mosaicsAppApp
 */
angular.module('mosaicsControllers')
  .controller('SparqlCtrl', ['$http', '$scope', 'AppState', function ($http, $scope, AppState) {
    $scope.query = 'CONSTRUCT {?a ?b ?c} WHERE {?a ?b ?c}';
    $scope.result = '';

    $scope.MIME_MAPPING = {
        rdf: "application/rdf+xml",
        jsonld : "application/ld+json",
        ttl : "text/turtle"
    }

    $scope.editorOptions = {
        lineWrapping : true,
        lineNumbers: true,
        //readOnly: 'nocursor',
        mode: 'sparql'
    }

    $scope.sampleQueries = {
        "Annotations sorted by date" : "/samples/queries/annotations.sparql",
        "Lucene indexing" : "/samples/queries/lucene.sparql"
    }

    $scope.loadQuery = function(url) {
        $http
            .get(url)
            .success(function(data) {
                $scope.query = data
            })
    }

    $scope.format = 'jsonld'

    $scope.$watch('format', function() {
        $scope.sendQuery()
    });

    $scope.sendQuery = function() {
        $http
            .get(AppState.apiUrl+'/query.'+$scope.format, {params: {sparql: $scope.query}, transformResponse: function(data) {return data}})
            .success(function(data) {
                $scope.result = data
            })
            .error(function(err) {
                $scope.result = err
            });
    }

    $scope.sendDelete = function() {
        $http
            .delete(AppState.apiUrl+'/query.'+$scope.format, {params: {sparql: $scope.query}, transformResponse: function(data) {return data}})
            .success(function(data) {
                $scope.result = data
            })
            .error(function(err) {
                $scope.result = err
            });
    }

    $scope.sendPut = function() {
        $http
            .put(AppState.apiUrl+'/query.'+$scope.format, $scope.query, { headers: {'Content-Type': $scope.MIME_MAPPING[$scope.format]}})
            .success(function(data) {
                $scope.result = data
            })
            .error(function(err) {
                $scope.result = err
            });
    }
  }]);


