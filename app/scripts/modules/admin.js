var searchControllers = angular.module('mosaicsAdminModule', ['ui.router'])

searchControllers.config(['$stateProvider', function($stateProvider) {
    $stateProvider
        .state('admin', {
            url: '/admin',
            template: '<div ui-view></div>'
        })
        .state('admin.users', {
            url: '/users',
            templateUrl: 'views/admin/users.html',
            controller: 'AdminUsersCtrl'
       }).state('admin.server', {
            url: '/server',
            templateUrl: 'views/admin/server.html',
            controller: 'AdminServerCtrl'
        })  .state('admin.config', {

    })
}])


searchControllers.controller('AdminUsersCtrl', ['$scope', 'User', function($scope, User) {

    $scope.users = User.query();

}])


searchControllers.controller('AdminServerCtrl', ['$scope', 'Server', function($scope, Server) {

    $scope.clean = function() {
        Server.clean()
    }

    $scope.initdata = function() {
        Server.initdata()
    }

}])

