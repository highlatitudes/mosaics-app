var searchControllers = angular.module('mosaicsSearchModule', ['ui.router'])

searchControllers.config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('search', {
        url: '/search?query',
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
    })
}])



searchControllers.directive('annotationSearchResult', function() {
    return {
        restrict: 'E',
        scope: {
            ann: '=',
            sref: '@'
        },
        controller: 'AnnotationSearchResultCtrl',
        templateUrl: 'views/annotationSearchResult.html',
        replace : true
    }
})


searchControllers.controller('AnnotationSearchResultCtrl', ['$scope', function($scope) {

    var dimensionClassMap = {}
    dimensionClassMap[FRAGVIZ.UTILS.MIME.DIMENSIONS.TEXT] = "fa fa-file-text-o";
    dimensionClassMap[FRAGVIZ.UTILS.MIME.DIMENSIONS.TABULAR] = "fa fa-table";
    dimensionClassMap[FRAGVIZ.UTILS.MIME.DIMENSIONS.SPATIAL] = "fa fa-map";
    dimensionClassMap[FRAGVIZ.UTILS.MIME.DIMENSIONS.VIDEO] = "fa fa-file-video-o";
    dimensionClassMap[FRAGVIZ.UTILS.MIME.DIMENSIONS.AUDIO] = "fa fa-file-audio-o";

    var targetViewerPromise = FRAGVIZ.VIEWERS.findMatchingViewer($scope.ann.target.source, $scope.ann.target.format)
    targetViewerPromise.then(function() {
        $scope.targetMimeType = targetViewerPromise.matchedMimeType;
        $scope.targetDimension = $scope.targetMimeType && FRAGVIZ.UTILS.MIME.getMimeDimension($scope.targetMimeType)
    })

    if ($scope.ann.body) {
        var bodyViewerPromise = FRAGVIZ.VIEWERS.findMatchingViewer($scope.ann.body.source, $scope.ann.body.format)
        bodyViewerPromise.then(function() {
            $scope.bodyMimeType = bodyViewerPromise.matchedMimeType;
            $scope.bodyDimension = $scope.bodyMimeType && FRAGVIZ.UTILS.MIME.getMimeDimension($scope.bodyMimeType)
        })
    }
    else {
        // no body - assumed to be a plain comment
    }

    $scope.getDimensionClass = function(dimension) {
        return dimensionClassMap[dimension] || "fa fa-file-o";
    }

    $scope.getLinkName = function(linkUri) {
        //TODO use vocabulary repo to get label
        return linkUri && linkUri.split("/").last()
    }

    $scope.truncateUri = function(uri, maxChars) {
        maxChars = maxChars || 50;

        if (uri && uri.length > maxChars) {
            return uri.substring(0, maxChars/2) + '…' + uri.substring(uri.length-(maxChars/2), uri.length);

        } else {
            return uri
        }

    }

}])


searchControllers.controller('SearchCtrl',
    ['$scope', 'AppState', '$http', 'Mosaic', 'Annotation', '$stateParams', '$state', '$rootScope','$timeout',
    function($scope, AppState, $http, Mosaic, Annotation, $stateParams, $state, $rootScope,$timeout) {
    $scope.searchForm = {text:$stateParams.query}

    $scope.$on('$locationChangeSuccess', function(event) {
        $scope.searchForm.text = $stateParams.query
    });

    $scope.recentMosaics = Mosaic.search();
    $scope.myRecentMosaics = $rootScope.currentUser && Mosaic.mine();
    $scope.recentAnnotations = Annotation.sortedSearch({ orderBy: 'modified', limit: 10});

    $scope.submitSearch = function() {
        if ($scope.searchForm.text && ($scope.searchForm.text.indexOf("http://") == 0 || $scope.searchForm.text.indexOf("https://") == 0)) {
            $state.transitionTo('preview', {url: $scope.searchForm.text});
        }
    }

    $scope.$watch("searchForm.text", function() {
        $scope.matchingResources = [];
        $scope.matchingAnnotations = [];
        $scope.matchingMosaics = [];

        $stateParams.query = $scope.searchForm.text //TODO wrong, should get updated statePAram in locatinChangeSuccess
        $state.transitionTo('search', {query: $scope.searchForm.text}, { notify: false });

        var searchParams = angular.copy($scope.searchForm)
        var annotationSearchParams = angular.copy($scope.searchForm)
        if (searchParams.text && (searchParams.text.indexOf("http://") == 0 || searchParams.text.indexOf("https://") == 0)) {
            searchParams.url = searchParams.text
            // have a separate set of search params for annotations, as API differs on url param structure
            // TODO support search on both body and target for annotations
            annotationSearchParams.target = {source: searchParams.text}
            searchParams.text = annotationSearchParams.text = undefined
        }

        if (!searchParams.url) { // TODO not very elegant
            $http.post(AppState.apiUrl + '/mosaics/search.json', searchParams)
                .then(function (res) {
                    $scope.matchingMosaics = res.data
                })
        }

        $http.post(AppState.apiUrl+'/resources/search.json', searchParams)
            .then(function(res) {
                $scope.matchingResources = res.data
            })

        $http.post(AppState.apiUrl+'/annotations/search.json', annotationSearchParams)
            .then(function(res) {
                $scope.matchingAnnotations = res.data
            })
    })
}])