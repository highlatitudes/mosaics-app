var searchDropdowns = angular.module('searchDropdownModule', [])

searchDropdowns.directive('searchDropdown', function() {
    return {
        restrict: 'E',
        scope: {
            displayField: '@',
            autocompleteSources: '=sources',
            modelAttr: '=ngModel',
            ngEnter: '&'
        },
        controller: 'SearchDropdownCtrl',
        templateUrl: 'views/search_dropdown.html'
    }
})


searchDropdowns.controller('SearchDropdownCtrl',
    ['$scope', '$element', '$templateRequest', '$compile', '$timeout',
    function($scope, $element, $templateRequest, $compile, $timeout) {

    $scope.selectedAcSource = $scope.autocompleteSources[0]

    var $input = $element.find('.dropdownInput')
    var currentQuery

    $scope.setACSource = function(source) {
        $scope.selectedAcSource = source
        $timeout(function() {
            $input.devbridgeAutocomplete().hide()
            $input.devbridgeAutocomplete().clear()
            $input.devbridgeAutocomplete().clearCache()
            $input.trigger("focus")
            //$(selector).devbridgeAutocomplete().suggest()
            //$(selector).parent().find(".searchCombo").show()
        }, 100)
    }

    $input
        .devbridgeAutocomplete({
            ajaxSettings: {traditional: true},
            triggerSelectOnValidInput : false,
            minChars: 3,
            appendTo: $element,
            showNoSuggestionNotice: true,
            preserveInput: false,
            serviceUrl: function(token) {return $scope.selectedAcSource.url},
            paramName: 'query',
            //dataType: 'jsonp',
            noSuggestionNotice: '<i>No Results</i>',
            onSearchStart: function(params) {
                currentQuery = params['query']
                $scope.selectedAcSource.preprocessParams && $scope.selectedAcSource.preprocessParams(params)
                $element.find(".spinner").show()
            },
            onSearchComplete: function(q, suggestions) {
                currentQuery = undefined
                $element.find(".spinner").hide()
                var searchCombo
                $templateRequest("views/searchComboTemplate.html").then(function(html){
                    searchCombo = angular.element(html);
                    $compile(searchCombo)($scope);
                    $element.find("div.autocomplete-suggestions").prepend(searchCombo)
                });
                $element.find("div.autocomplete-suggestions").css({"min-width": $input.outerWidth()});
            },
            onSearchError: function(q, jqXHR, textStatus, errorThrown) {
                if (q == currentQuery) // hide spinner only if the error is about the current query
                    $element.find(".spinner").hide()
            },

            formatResult: function(suggestion, currentValue) {
                return "<div><i class='mime-icon' data-mimetype='"+suggestion.mimetype+"'/><span>"+suggestion.value+"</span></div>"
            },

            onHide: function(container) {
                //searchCombo.detach()
            },

            transformResult: function(response) {
                return $scope.selectedAcSource.resultTransformer(response)
            },

            onSelect: function(suggestion) {
                $timeout(function() {
                    $scope.selectedAcSource.setResult(suggestion, $scope.modelAttr)
                    $scope.ngEnter && $scope.ngEnter()
                })
            }
        })
}])