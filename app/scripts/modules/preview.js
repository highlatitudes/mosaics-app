var previewControllers = angular.module('mosaicsPreviewModule', ['ui.router'])

previewControllers.config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('preview', {
        url: '/preview?url?ann',
        templateUrl: 'views/preview.html',
        controller: 'PreviewCtrl'
    })
}])

previewControllers.controller('PreviewCtrl',
    ['$rootScope', '$scope', 'AppState', '$http', 'Mosaic', '$stateParams', '$state', 'Annotation',
    function($rootScope, $scope, AppState, $http, Mosaic, $stateParams, $state, Annotation) {

    $scope.autocompleteSources = $rootScope.catalogSources

    $scope.typeMap = _.extend({}, {"auto":"Auto"}, FRAGVIZ.UTILS.MIME.typeMap)
    $scope.requiredMimeType = "auto"

    var url = $stateParams['url']

    $scope.annotationFilter = {
        owner: $rootScope.currentUser ? 'me' : '',
        target: {
            source: url
        }
    }

    // curViewer should be set as this directive is embedded in a resource-context directive
    $scope.$watch("curViewer.matchedMimeType", function(mt) {
        $scope.typeMap['auto'] = "Auto ("+FRAGVIZ.UTILS.MIME.typeMap[mt]+")"
    })

    $scope.formResource = {
        "source" : url,
        "@type": "moz:Resource",
        "dc:title": "New Resource"
    }

    $scope.tempMosaic = {
        "resources": [angular.extend({}, $scope.formResource)],
        "annotations": [],
        "@type": "moz:Mosaic",
        "dc:title": "New Mosaic"
    }

    $scope.$on("hilats.add_annotation", function(event, newAnnot) {
        Annotation.save({}, newAnnot, function(obj) {
            //TODO something ?
            obj
        })
    })


    $scope.preview = function(mimetype) {

        if (mimetype == 'auto')
            mimetype = undefined

        $scope.formResource.format = mimetype;

        $scope.tempMosaic.resources[0] = angular.extend({}, $scope.formResource)

        $state.transitionTo('preview', {url: $scope.tempMosaic.resources[0].source}, { notify: false });

        AppState.status.promise.then(function () {
            $scope.selectedRes = $scope.tempMosaic.resources[0]
        })
    }

    $scope.preview()

}])