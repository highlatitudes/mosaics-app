var previewControllers = angular.module('mosaicsProfileModule', ['ui.router'])

previewControllers.config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('myprofile', {
        url: '/myprofile',
        templateUrl: 'views/profile.html',
        controller: 'MyProfileCtrl',
        resolve: {
            currentUser: function (User) {
                return User.current()
            }
        }
    })
}])

previewControllers.controller('MyProfileCtrl', [ '$scope', 'AppState', '$stateParams', '$state', function($scope, AppState, $stateParams, $state) {


}])