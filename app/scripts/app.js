'use strict';

/**
 * @ngdoc overview
 * @name mosaicsAppApp
 * @description
 * # mosaicsAppApp
 *
 * Main module of the application.
 */

(function() {


    /**************************************************************************
     * Set environment values
     *************************************************************************/

    var app = angular
        .module('mosaicsDefaultApp', [
            'mosaicsApp'
        ])
        .config(['$urlRouterProvider', '$stateProvider', '$httpProvider', function ($urlRouterProvider, $stateProvider, $httpProvider) {

            // Caching causes REST API calls to be cached
            //$httpProvider.defaults.cache = true

            $urlRouterProvider.otherwise('/search');

            $stateProvider
                .state('main', {
                    url: "/",
                    templateUrl: "views/main.html",
                    reloadOnSearch: false
                })
                .state('mosaics', {
                    url: '/mosaics',
                    templateUrl: 'views/query.html',
                    //controller: 'MosaicsCtrl',
                    reloadOnSearch: false
                })
                .state('mosaics.query', {
                    url: '/search/:query',
                    templateUrl: 'views/query.html',
                    //controller: 'MosaicsCtrl',
                    reloadOnSearch: false
                })
                .state('mosaics.all', {
                    url: '',
                    templateUrl: 'views/query.html',
                    //controller: 'MosaicsCtrl',
                    reloadOnSearch: false
                })
                .state('mosaics.id', {
                    url: '/:id',
                    templateUrl: 'views/query.html',
                    //controller: 'MosaicsCtrl',
                    reloadOnSearch: false
                })
                .state('mosaics.id.annotation', {
                    url: '/annot/:ann?open',
                    templateUrl: 'views/query.html',
                    //controller: 'MosaicsCtrl',
                    reloadOnSearch: false
                })
                .state('mosaics.id.resource', {
                    url: '/res/:res',
                    templateUrl: 'views/query.html',
                    //controller: 'MosaicsCtrl',
                    reloadOnSearch: false
                })
                .state('about', {
                    url: '/about',
                    templateUrl: 'views/about.html'
                })
                .state('doc', {
                    url: '/doc',
                    templateUrl: 'views/doc.html'
                })
                /*
                 .state('samples', {
                 url: '/samples',
                 templateUrl: 'views/samples.html',
                 reloadOnSearch: false
                 })
                 */
                .state('samples', {
                    url: '/samples/:sampleId',
                    templateUrl: function(stateParams){
                        return 'samples/' + stateParams.sampleId + '.html';
                    },
                    reloadOnSearch: false
                })
                .state('sparql', {
                    url: '/sparql',
                    templateUrl: 'views/sparql.html',
                    controller: 'SparqlCtrl'
                })

        }])

        .run(['$rootScope', 'AppState', function($rootScope, AppState) {

            $rootScope.displayDisclaimer = !$rootScope.getCookie('disclaimerRead')

            $rootScope.catalogSources = [
                {   name: "Resource",
                    url: AppState.apiUrl + '/resources.json',
                    resultTransformer: function (response) {
                        var results = angular.fromJson(response)
                        return {
                            suggestions: $.map(results, function (result) {
                                return { value: result['dc:title'], data: result, mimetype: result.format && result.format.toLowerCase() };
                            })
                        };
                    },
                    setResult: function (suggestion, res) {
                        res.source = suggestion.data.source
                        if (suggestion.data.selector)
                            res.selector = {
                                type: suggestion.data.selector.type,
                                value: suggestion.data.selector.value
                            }
                    },
                    preprocessParams: function (params) {
                        //params['query'] = params['query']
                        params['expanded'] = true
                    }
                },
                {   name: "OpenBelgium",
                    url: FRAGVIZ.UTILS.proxyUrl + "?_uri=" + encodeURI("http://portal.openbelgium.be/api/action/resource_search"),
                    resultTransformer: function (response) {
                        var result = angular.fromJson(response)
                        return {
                            suggestions: $.map(result.result.results, function (result) {
                                return { value: result.name, data: result, mimetype: result.format && result.format.toLowerCase()};
                            })
                        };
                    },
                    setResult: function (suggestion, res) {
                        res.source = suggestion.data.url
                        res.format = FRAGVIZ.UTILS.MIME.getExtensionMimeType(suggestion.data.format) || suggestion.data.format
                    },
                    preprocessParams: function (params) {
                        var queryParams = params['query'].split(' ')
                        queryParams = queryParams.map(function(queryParam) {
                            if (queryParam.indexOf(':')>0)
                                return queryParam
                            else
                                return "name:" + queryParam
                        })
                        params['query'] = queryParams
                    }
                },
                {   name: "Brussels",
                    url: FRAGVIZ.UTILS.proxyUrl + "?_uri=" + encodeURI("http://opendatastore.brussels/api/action/resource_search"),
                    resultTransformer: function (response) {
                        var result = angular.fromJson(response)
                        return {
                            suggestions: $.map(result.result.results, function (result) {
                                return { value: result.name, data: result, mimetype: result.format && result.format.toLowerCase() };
                            })
                        };
                    },
                    setResult: function (suggestion, res) {
                        res.source = suggestion.data.url
                        res.format = FRAGVIZ.UTILS.MIME.getExtensionMimeType(suggestion.data.format) || suggestion.data.format
                    },
                    preprocessParams: function (params) {
                        var queryParams = params['query'].split(' ')
                        queryParams = queryParams.map(function(queryParam) {
                            if (queryParam.indexOf(':')>0)
                                return queryParam
                            else
                                return "name:" + queryParam
                        })
                        params['query'] = queryParams
                        params['limit'] = 50
                    }
                },
                {   name: "Vlaanderen",
                    url: FRAGVIZ.UTILS.proxyUrl + "?_uri=" + encodeURI("http://opendata.vlaanderen.be/api/action/resource_search"),
                    resultTransformer: function (response) {
                        var result = angular.fromJson(response)
                        return {
                            suggestions: $.map(result.result.results, function (result) {
                                return { value: result.name, data: result, mimetype: result.format && result.format.toLowerCase() };
                            })
                        };
                    },
                    setResult: function (suggestion, res) {
                        res.source = suggestion.data.url
                        res.format = FRAGVIZ.UTILS.MIME.getExtensionMimeType(suggestion.data.format) || suggestion.data.format
                    },
                    preprocessParams: function (params) {
                        var queryParams = params['query'].split(' ')
                        queryParams = queryParams.map(function(queryParam) {
                            if (queryParam.indexOf(':')>0)
                                return queryParam
                            else
                                return "name:" + queryParam
                        })
                        params['query'] = queryParams
                        params['limit'] = 50
                    }
                },
                {   name: "Wallonie",
                    url: FRAGVIZ.UTILS.proxyUrl + "?_uri=" + encodeURI("http://data.digitalwallonia.be/api/action/resource_search"),
                    resultTransformer: function (response) {
                        var result = angular.fromJson(response)
                        return {
                            suggestions: $.map(result.result.results, function (result) {
                                return { value: result.name, data: result, mimetype: result.format && result.format.toLowerCase() };
                            })
                        };
                    },
                    setResult: function (suggestion, res) {
                        res.source = suggestion.data.url
                        res.format = FRAGVIZ.UTILS.MIME.getExtensionMimeType(suggestion.data.format) || suggestion.data.format
                    },
                    preprocessParams: function (params) {
                        var queryParams = params['query'].split(' ')
                        queryParams = queryParams.map(function(queryParam) {
                            if (queryParam.indexOf(':')>0)
                                return queryParam
                            else
                                return "name:" + queryParam
                        })
                        params['query'] = queryParams
                        params['limit'] = 50
                    }
                },
                {   name: "JRC CKAN",
                    url: FRAGVIZ.UTILS.proxyUrl + "?_uri=" + encodeURI("http://data.jrc.ec.europa.eu/api/action/resource_search"),
                    resultTransformer: function (response) {
                        var result = angular.fromJson(response)
                        return {
                            suggestions: $.map(result.result.results, function (result) {
                                return { value: result.name, data: result, mimetype: result.format && result.format.toLowerCase() };
                            })
                        };
                    },
                    setResult: function (suggestion, res) {
                        res.source = suggestion.data.url
                        res.format = FRAGVIZ.UTILS.MIME.getExtensionMimeType(suggestion.data.format) || suggestion.data.format
                    },
                    preprocessParams: function (params) {
                        var queryParams = params['query'].split(' ')
                        queryParams = queryParams.map(function(queryParam) {
                            if (queryParam.indexOf(':')>0)
                                return queryParam
                            else
                                return "name:" + queryParam
                        })
                        params['query'] = queryParams
                        params['limit'] = 50
                    }
                },
                {   name: "EDP",
                    url: FRAGVIZ.UTILS.proxyUrl + "?_uri=" + encodeURI("https://www.europeandataportal.eu/data/fr/api/action/resource_search"),
                    resultTransformer: function (response) {
                        var result = angular.fromJson(response)
                        return {
                            suggestions: $.map(result.result.results, function (result) {
                                if (result.format && result.format.startsWith("OGC:"))
                                    result.format = result.format.substring(4)
                                return { value: result.name, data: result, mimetype: result.format && result.format.toLowerCase() };
                            })
                        };
                    },
                    setResult: function (suggestion, res) {
                        res.source = suggestion.data.url
                        res.format = FRAGVIZ.UTILS.MIME.getExtensionMimeType(suggestion.data.format) || suggestion.data.format
                    },
                    preprocessParams: function (params) {
                        var queryParams = params['query'].split(' ')
                        queryParams = queryParams.map(function(queryParam) {
                            if (queryParam.indexOf(':')>0)
                                return queryParam
                            else
                                return "name:" + queryParam
                        })
                        params['query'] = queryParams
                        params['limit'] = 50
                    }
                }
            ]
        }])

    app.directive("disclaimer", function() {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "views/disclaimer.html"
        };
    });
})()


