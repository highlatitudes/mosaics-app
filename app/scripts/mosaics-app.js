'use strict';

(function() {

    var authProvider = null

    var app = angular
        .module('mosaicsApp', [
            'mosaicsPreviewModule',
            'mosaicsSearchModule',
            'mosaicsProfileModule',
            'searchDropdownModule',
            'mosaicsAdminModule',
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
            'mosaicsControllers',
            'mosaicsServices',
            'ui.codemirror',
            'ui.bootstrap',
            'satellizer',
            'ui.router',
            'ngCookies'
        ])
        .factory('AppState', ['$q', function($q) {
            return {
                status : $q.defer(),
                apiUrl : '/api'
                //apiUrl : '/samples'
            };
        }])
        .config(
        ['$urlRouterProvider', '$authProvider', '$stateProvider', '$httpProvider',
        function ($urlRouterProvider, $authProvider, $stateProvider, $httpProvider) {

            $httpProvider.interceptors.push('httpRequestInterceptor');

            authProvider = $authProvider
        }])

        .run(
        ['$auth', '$rootScope', 'Authentication', '$http', 'AppState', '$cookies',
        function($auth, $rootScope, Authentication, $http, AppState, $cookies) {

            $http.get(AppState.apiUrl + '/auth/providers.json')
                .then(function(res) {
                    var providersConfig = res.data;
                    angular.forEach(providersConfig, function (conf, providerName) {
                        var providerFunc = authProvider[providerName];
                        if (!providerFunc)
                            throw "OAuth provider not found " + providerName
                        providerFunc({
                            clientId: conf.client_id,
                            url: '/api/auth/' + providerName
                            //authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
                            //redirectUri: window.location.origin,
                            //requiredUrlParams: ['scope'],
                            //optionalUrlParams: ['display'],
                            //scope: ['profile', 'email'],
                            //scopePrefix: 'openid',
                            //scopeDelimiter: ' ',
                            //display: 'popup',
                            //type: '2.0',
                            //popupOptions: { width: 452, height: 633 }
                        })
                    })
                })

            $http.get(AppState.apiUrl + '/server/config.json')
                .then(function(res) {
                    var serverConfig = res.data;

                    if (serverConfig.proxyPort && serverConfig.proxyPort != -1) {
                        //FRAGVIZ.UTILS.proxyUrl = 'http://server.mosaics.highlatitud.es/proxy'
                        var loc = window.location;
                        FRAGVIZ.UTILS.proxyUrl = loc.protocol+ '//' + loc.hostname +':' +serverConfig.proxyPort + '/proxy';
                    } else {
                        FRAGVIZ.UTILS.proxyUrl = '/proxy';
                    }

                    if (serverConfig.basemaps)
                        FRAGVIZ.VIEWERS.defaultBasemaps = serverConfig.basemaps

                    // resolve AppState as done
                    // TODO there must be a better place to do this
                    AppState.status.resolve(true)
                })

            $rootScope.getCookie = function(key) {
                return $cookies.get(key);
            }

            $rootScope.setCookie = function(key, value) {
                return $cookies.put(key, value);
            }

            $rootScope.auth = $auth
            $rootScope.$watch("auth.isAuthenticated()", function(isAuthenticated) {
                $rootScope.currentUser = isAuthenticated && Authentication.get()
            })

            $rootScope.currentUserHasRole = function(role) {
                return $rootScope.currentUser && $rootScope.currentUser.authenticated &&
                       $rootScope.currentUser.principal.user.roles.indexOf(role) >= 0
            }

            $rootScope.hasPermission = function(permission, obj) {
                //TODO implement finer authorization scheme
                if (permission == 'read') {
                    // currently everything is public
                    return true
                } else if (permission == 'write' || permission == 'delete') {
                    // must be owner to write or delete
                    return $rootScope.currentUser &&
                           $rootScope.currentUser.authenticated &&
                           (  $rootScope.currentUserHasRole('admin') ||
                                  obj && obj['dc:creator'] &&
                                  obj['dc:creator'] == $rootScope.currentUser.id )
                } if (permission == 'admin') {
                    return $rootScope.currentUserHasRole('admin')
                } else
                    throw "Unknown permission "+permission;
            }

            $rootScope.keys = Object.keys // make keys iterator available for use in angular templates (ng-show, ng-if, ...)


            $rootScope.catalogSources = [
                {   name: "Resource",
                    url: AppState.apiUrl + '/resources.json',
                    resultTransformer: function (response) {
                        var results = angular.fromJson(response)
                        return {
                            suggestions: $.map(results, function (result) {
                                return { value: result['dc:title'], data: result, mimetype: result.format && result.format.toLowerCase() };
                            })
                        };
                    },
                    setResult: function (suggestion, res) {
                        res.source = suggestion.data.source
                        if (suggestion.data.selector)
                            res.selector = {
                                type: suggestion.data.selector.type,
                                value: suggestion.data.selector.value
                            }
                    },
                    preprocessParams: function (params) {
                        //params['query'] = params['query']
                        params['expanded'] = true
                    }
                }
            ]
        }])


    app.factory('RecentErrors', function () {
        return {
            new_errors: [],
            log: [],

            addError: function(err) {
                this.new_errors.push(err)
            },

            flushErrors: function() {
                this.log = this.log.concat(this.new_errors)
                this.new_errors = []
            }
        };
    });

    app.factory('httpRequestInterceptor', ['$q', '$location', 'RecentErrors', function ($q, $location, RecentErrors) {
        return {
            'responseError': function(rejection) {
                // do something on error
                RecentErrors.addError(rejection)
                return $q.reject(rejection);
            }
        };
    }]);

    app.directive("appErrors", function() {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "views/appErrors.html",
            controller: "ErrorCtrl"
        };
    });

    app.controller('ErrorCtrl', ['$scope', 'RecentErrors', function ($scope, RecentErrors) {
        $scope.appErrors = RecentErrors

        $scope.showErrors = false

        $scope.$watchCollection('appErrors.new_errors', function() {
            if ($scope.appErrors.new_errors.length > 0) {
                $scope.toggleDisplay(true)
            }
        })

        $scope.toggleDisplay = function(toggle) {
            if (toggle === undefined)
                toggle = !$scope.showErrors

            $scope.showErrors = toggle

            if (!toggle)
                $scope.appErrors.flushErrors();
        }
    }]);

    app.directive("topMenu", function() {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "snippets/topmenu.html"
        };
    });

    /*
     This directive allows us to pass a function in on an enter key to do what we want.
     From http://ericsaupe.com/angularjs-detect-enter-key-ngenter/
     */
    app.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });

    app.directive('editable', function () {
        return {
            restrict: 'AE',
            replace: false,
            /*terminal: true,
             priority: 1000,*/
            scope: {
                ngModel: "="
            },
            template:"<span class='editable' ng-init='edit=false' ng-dblclick='edit = !edit' ng-class='{edited: edit}'><input ng-blur='edit=false' ng-disabled='!edit' size='{{ngModel && (ngModel.length + 3)}}' ng-enter='edit=false' ng-model='ngModel'></span>",

            link: function link(scope, element, attrs) {

                scope.$watch("edit", function(value) {
                    var input = element.children().children()[0]
                    if (value) input.focus()
                    else input.blur()
                })
            }

        };
    })

    window.FRAGVIZ.styles = {
        include : "http://localhost:9000/libs/fragviz_include/styles/include.css"
    };

    PDFJS.workerSrc  = 'libs/pdfjs-dist/build/pdf.worker.js'
    OpenLayers.ImgPath = "libs/openlayers2/img/"
    OpenLayers.ThemePath = "libs/openlayers2/theme/default"

})()


