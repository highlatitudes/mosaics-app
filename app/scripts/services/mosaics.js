'use strict'

var mosaicsService = angular.module('mosaicsServices', ['ngResource']);

mosaicsService
    .factory('Mosaic', ['$resource', 'AppState',
    function($resource, AppState){
        var res = $resource(
                AppState.apiUrl+'/mosaics/:id.json',
                {id: '@id'},
                {
                    get: {method:'GET'},
                    mine: {method:'GET', isArray:true, url: AppState.apiUrl+'/mosaics/mine.json'},
                    create: {method:'POST', isArray:true},
                    update: {method:'PUT'},
                    delete: {method:'DELETE'},
                    query: {method:'GET', isArray:true},   // TODO merge get, query and/or search
                    search: {method:'POST', isArray:true, url: AppState.apiUrl+'/mosaics/search.json'}
                })

        res.save = function(params, obj, callback) {
            var id = (params && params.id) || obj.id

            var $Mosaic = this

            if (id)
                return this.update(params, obj, callback)
            else
                return this.create(params, obj,
                    function(objs) {
                        // create returns an array of ids
                        // assume there's only one and fetch it
                        //TODO check
                        $Mosaic.get({id: objs[0]}, callback)
                    })
        };

        res.fromUrl = function(url) {
            return $resource(url).get()
        }

        return res
    }
    ])

    .factory('Annotation', ['$resource', 'AppState',
        function($resource, AppState){
            var res = $resource(
                    AppState.apiUrl+'/annotations/:id.json',
                {id: '@id'},
                {
                    get: {method:'GET'},
                    mine: {method:'GET', isArray:true, url: AppState.apiUrl+'/annotations/mine.json'},
                    create: {method:'POST', isArray:true},
                    update: {method:'PUT'},
                    delete: {method:'DELETE'},
                    query: {method:'GET', isArray:true},   // TODO merge get, query and/or search
                    search: {
                        method:'POST',
                        isArray:true,
                        url: AppState.apiUrl+'/annotations/search.json'}
                })

            res.sortedSearch = function(params) {
                var results = this.search(params);
                if (params.orderBy) {
                    results.$promise = results.$promise.then(function() {
                        return results.sort(function(obj1, obj2) {
                            if (obj1[params.orderBy] == obj2[params.orderBy])
                                return 0
                            else if (obj1[params.orderBy] === undefined)
                                return 1
                            else if (obj2[params.orderBy] === undefined)
                                return -1
                            else
                                return obj1[params.orderBy] >= obj2[params.orderBy] ? -1 : 1
                        })
                    })
                }

                return results;
            };

            res.save = function(params, obj, callback) {
                var id = (params && params.id) || obj.id

                var $Annotation = this

                if (id)
                    return this.update(params, obj, callback)
                else
                    return this.create(params, obj,
                        function(objs) {
                            // create returns an array of ids
                            // assume there's only one and fetch it
                            //TODO check
                            $Annotation.get({id: objs[0]}, callback)
                        })
            };

            res.fromUrl = function(url) {
                return $resource(url).get()
            }

            return res
        }
    ])

    .factory('User',
    ['$resource', 'AppState', function ($resource, AppState) {
        return $resource(
            AppState.apiUrl+'/user/:name',
            {name:'@id'},
            {
                //get: {method:'GET'},
                current: {method:'GET', url: AppState.apiUrl+'/user/current.json'},
                update: {method:'PUT'},
                //query: {method:'GET', isArray:true},
                //delete: {method:'DELETE'}
            });

    }])

    .factory('Server',
    ['$http', 'AppState', function ($http, AppState) {
        return {
            clean: function() {
                return $http({
                    method: 'POST',
                    url: AppState.apiUrl+'/server/exec',
                    params: {
                        action: 'clean'
                    }
                })
            },
            initdata: function() {
                return $http({
                    method: 'POST',
                    url: AppState.apiUrl+'/server/exec',
                    params: {
                        action: 'initdata'
                    }
                })
            }

        }

    }])

    .factory('Authentication', ['$resource', 'AppState',
    function ($resource, AppState) {
        return $resource(AppState.apiUrl+'/auth');

    }])