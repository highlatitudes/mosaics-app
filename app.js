var express = require('express');
var httpProxy = require('http-proxy');
var url = require('url');

var port = process.env.PORT || 3000;
var app = express();

var DEBUG = false

var proxy = httpProxy.createProxyServer();
proxy.on('error', function(e) {
    console.log(e);
});
if (DEBUG)
    proxy.on('proxyRes', function (proxyRes, req, res) {
        console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2));
    });



function proxyReq() {
    return function(req, res) {
        var incomingUri = url.parse(req.url, true);

        if (DEBUG) {
            console.log("INCOMING: " + req.url);
            for (var idx in req.headers)
                console.log("INCOMING: " + idx + "=" + req.headers[idx]);
        }

        var targetUri = url.parse(incomingUri.query._uri, true)

        for (var param in incomingUri.query) {
            if (DEBUG) console.log("QUERYPARAM: "+param+"="+incomingUri.query[param])
            if (param != '_uri') targetUri.query[param] = incomingUri.query[param]
        }
        targetUri.search = undefined // remove search to be sure query obj is used
        req.url=url.format(targetUri)
        if (DEBUG) console.log("NEW: "+req.url);
        req.hostname=targetUri.host
        req.headers['host'] = targetUri.host
        var target = targetUri.protocol+"//"+targetUri.host
        if (DEBUG) console.log("TARGET: "+target);
        proxy.web(req, res,
            /*{ target: "http://localhost:3000/test" }*/
            { target: target }
        );
    }
}

function displayReq() {
    return function(req, res) {
        var url_parts = url.parse(req.url, true);
        console.log("TEST INCOMING: "+req.url);
        console.log("TEST INCOMING: "+req.host);
        for (var idx in req.headers)
            console.log("TEST INCOMING: "+idx+"="+req.headers[idx]);
    }
}


app.use(express.static(__dirname + '/dist'));
//app.use(proxyReq());
app.all("/proxy?*", proxyReq())
app.all("/test/*", displayReq())

app.listen(port);