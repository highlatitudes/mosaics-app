describe('Mosaics controllers', function() {

    beforeEach(function(){
        this.addMatchers({
            toEqualData: function(expected) {
                return angular.equals(this.actual, expected);
            }
        });

    });

    beforeEach(module('mosaicsAppApp'));
    //beforeEach(module('mosaicsControllers'));
    //beforeEach(module('mosaicsServices'));


    describe('MosaicListCtrl', function(){
        var scope, ctrl, tester;
        var mozService;

        beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
            tester = ngMidwayTester('mosaicsAppApp');

            //$httpBackend = _$httpBackend_;
            //$httpBackend.expectGET()
            //    respond([{name: 'Nexus S'}, {name: 'Motorola DROID'}]);

            scope = $rootScope.$new();
            mozService = tester.inject('Mosaic');


            mozService.save(
                {"@type" : "moz:Mosaic",
                 "dc:creator" : "moz:user1",
                 "resources": []})


            ctrl = $controller('MosaicListCtrl', {$scope: scope, Mosaic: mozService});

        }));

        afterEach(function() {
            tester.destroy();
            tester = null;
        });


        it('should create "phones" model with 2 phones fetched from xhr', function() {
            scope.mosaics.then(
                function(value) {
                expect(value).toEqualData([]);
            }, function(error) {
                    alert(error);
                })


            //$httpBackend.flush();
            //expect(scope.mosaics).toEqualData([{name: 'Nexus S'}, {name: 'Motorola DROID'}]);
        });

    });

});